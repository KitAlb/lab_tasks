package com.epam.lab.flower_shop.bouquet.decorator.implementation;

import com.epam.lab.flower_shop.bouquet.Bouquet;
import com.epam.lab.flower_shop.bouquet.decorator.BouquetDecorator;
import com.epam.lab.flower_shop.decorations.LoveDecoration;

public class ValentineDecorator extends BouquetDecorator {
    public ValentineDecorator(Bouquet bouquet) {
        super(bouquet);
        addDecoration(new LoveDecoration());
    }
}
