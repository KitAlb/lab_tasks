package com.epam.lab.Ship_With_Droids;

import org.apache.commons.lang3.StringUtils;

public class BB8 extends Droid {
    public BB8() {
        super("BB-8", 30);
        this.serialNumber = "bb8sn";
    }

    @Override
    public int compareTo(Droid o) {
        return StringUtils.compare(this.name, o.name);
    }
}
