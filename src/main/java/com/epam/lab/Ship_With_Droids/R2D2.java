package com.epam.lab.Ship_With_Droids;

import org.apache.commons.lang3.StringUtils;

public class R2D2 extends Droid {
    public R2D2() {
        super("R2D2", 20);
        this.serialNumber = "r2d2sn";
    }

    @Override
    public int compareTo(Droid o) {
        return StringUtils.compare(this.name, o.name);
    }

}
