package com.epam.lab.flower_shop.decorations;

public class PaperWrapping implements Decoration {
    @Override
    public double getPrice() {
        return 3;
    }
}
