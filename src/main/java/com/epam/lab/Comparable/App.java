package com.epam.lab.Comparable;

import com.epam.lab.Comparable.boardgame.Boardgame;
import com.epam.lab.Comparable.comparator.PublisherComparator;
import com.epam.lab.Comparable.generator.BoardgameGenerator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class App {
    private static Logger logger = LogManager.getLogger("ConsoleLogger");

    public static void main(String[] args) {
        logger.info("Unsorted Array of Boardgames:");
        Boardgame[] bgArray = new Boardgame[]{
                BoardgameGenerator.getRandomBoardgame(),
                BoardgameGenerator.getRandomBoardgame(),
                BoardgameGenerator.getRandomBoardgame(),
                BoardgameGenerator.getRandomBoardgame(),
                BoardgameGenerator.getRandomBoardgame()
        };
        logger.info(Arrays.toString(bgArray));
        logger.info("Sorted by Name Array of Boardgames:");
        Arrays.sort(bgArray);
        logger.info(Arrays.toString(bgArray));

        logger.info("Unsorted ArrayList of Boardgames:");
        ArrayList<Boardgame> bgArrayList = new ArrayList<>(Arrays.asList(
                BoardgameGenerator.getRandomBoardgame(),
                BoardgameGenerator.getRandomBoardgame(),
                BoardgameGenerator.getRandomBoardgame(),
                BoardgameGenerator.getRandomBoardgame(),
                BoardgameGenerator.getRandomBoardgame()
        ));
        logger.info(bgArrayList);
        logger.info("Sorted by Publisher ArrayList of Boardgames:");
        bgArrayList.sort(new PublisherComparator());
        logger.info(bgArrayList);

        Boardgame game = bgArrayList.get(3);
        logger.info("Search for element: " + game);
        logger.info("Element index is " + Collections.binarySearch(bgArrayList, game, new PublisherComparator()));

    }

}
