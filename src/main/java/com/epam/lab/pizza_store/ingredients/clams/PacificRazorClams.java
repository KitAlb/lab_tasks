package com.epam.lab.pizza_store.ingredients.clams;

public class PacificRazorClams implements Clams {
    @Override
    public String toString() {
        return "Pacific Razor";
    }
}
