package com.epam.lab.flower_shop.order.state;

import com.epam.lab.flower_shop.order.Order;

public interface State {
    default String create(Order order) {
        return "create - is not allowed";
    }

    default String pay(Order order) {
        return "pay - is not allowed";
    }

    default String prepare(Order order) {
        return "prepare - is not allowed";
    }

    default String deliver(Order order) {
        return "deliver - is not allowed";
    }

    default String close(Order order) {
        return "close - is not allowed";
    }

    default String cancel(Order order) {
        return "cancel is not allowed";
    }
}
