package com.epam.lab.game.kalaha.wire.player;

import com.epam.lab.game.kalaha.GameState;

public interface Player {

    int takeTurn(GameState state);

}
