package com.epam.lab.Ship_With_Droids;

import java.util.ArrayList;
import java.util.List;

public class Ship {
    private ArrayList<Droid> droids = new ArrayList<>();

    void putDroids(List<? extends Droid> droids) {
        this.droids.addAll(droids);
    }

    Droid getDroid(int index) {
        return this.droids.get(index);
    }

}
