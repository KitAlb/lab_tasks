package com.epam.lab.jdbc.controller;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;

@FunctionalInterface
public interface Printable {
    void print() throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException;
}
