package com.epam.lab.xml;

public enum SoilType {
    LOAM,
    SILT,
    CLAY,
    SAND
}
