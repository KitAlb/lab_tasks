package com.epam.lab.json.orangery;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum MultiplyingType {
    @JsonProperty("cuttings")
    CUTTINGS,
    @JsonProperty("leaves")
    LEAVES,
    @JsonProperty("seeds")
    SEEDS
}
