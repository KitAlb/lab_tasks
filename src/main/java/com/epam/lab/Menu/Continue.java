package com.epam.lab.Menu;

import java.util.Arrays;

public enum Continue {
    YES("y"),
    NO("n");

    private String value;

    Continue(String value) {
        this.value = value;
    }

    public static Continue of(String continueString) {
        return Arrays.stream(Continue.values())
                .filter(item -> item.value.equalsIgnoreCase(continueString) || continueString.startsWith(item.value))
                .findFirst()
                .orElse(Continue.NO);
    }

    @Override
    public String toString() {
        return value;
    }
}
