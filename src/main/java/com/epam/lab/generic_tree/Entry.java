package com.epam.lab.generic_tree;

import java.util.Comparator;

public class Entry<K, V> {

    K key;
    V value;
    Entry<K, V> left;
    Entry<K, V> right;
    private final Comparator<K> comparator;

    Entry(K key, V value, Comparator<K> comparator) {
        this.key = key;
        this.value = value;
        this.comparator = comparator;
    }

    public void put(K key, V value) {
        if (comparator.compare(key, this.key) < 0) {
            if (left != null) {
                left.put(key, value);
            } else {
                left = new Entry<K, V>(key, value, comparator);
            }
        } else if (comparator.compare(key, this.key) > 0) {
            if (right != null) {
                right.put(key, value);
            } else {
                right = new Entry<K, V>(key, value, comparator);
            }
        } else {
            this.value = value;
        }
    }

    public V get(K key) {
        if (this.key.equals(key)) {
            return value;
        }

        if (comparator.compare(key, this.key) < 0) {
            return left == null ? null : left.get(key);
        } else {
            return right == null ? null : right.get(key);
        }
    }
}
