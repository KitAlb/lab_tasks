package com.epam.lab.io;

import com.google.common.io.Files;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

public class FileReader {

    private Logger logger;
    private static String FILE_NAME = "src/main/resources/200MBFile.txt";
    private static String ONE_LINE_COMMENT_START = "//";
    private static String MULTILINE_COMMENT_START = "/*";
    private static String MULTILINE_COMMENT_END = "*/";
    private static String QUOTES = "\"";
    private static String REPLACEMENT = "X";

    public FileReader(Logger logger) {
        this.logger = logger;
    }

    /**
     * Read file with default Reader and count reading time
     */
    public void readFileDefault() {
        StopWatch stopWatch = new StopWatch();

        try (Reader reader = new java.io.FileReader(FILE_NAME)) {
            stopWatch.start();
            int i = reader.read();
            while (i != -1) {
                i = reader.read();
            }
            stopWatch.stop();
            logger.debug("Default reader time: {} ms", stopWatch.getTime(TimeUnit.MILLISECONDS));
        } catch (Exception ex) {
            logger.error("Cannot read file {}: {}", FILE_NAME, ex.getMessage());
        }
    }

    /**
     * Read file with default BufferedReader and count reading time
     *
     * @param bufferSize A BufferedReader's buffer size
     */
    public void readFileBuffered(int bufferSize) {
        StopWatch stopWatch = new StopWatch();

        try (BufferedReader reader = new BufferedReader(new java.io.FileReader(FILE_NAME), bufferSize)) {
            stopWatch.start();
            int i = reader.read();
            while (i != -1) {
                i = reader.read();
            }
            stopWatch.stop();
            logger.debug("Buffered reader time (buffer size is {}): {} ms", bufferSize, stopWatch.getTime(TimeUnit.MILLISECONDS));
        } catch (Exception ex) {
            logger.error("Cannot read file {}: {}", FILE_NAME, ex.getMessage());
        }
    }

    public List<String> getComments(String filePath) {
        List<String> comments = new ArrayList<>();
        boolean findCommentStart = false;
        try {
            // read all lines from file
            for (String string : Files.readLines(Path.of(filePath).toFile(), Charset.defaultCharset())) {
                string = StringUtils.trim(string);
                // one line comment
                if (StringUtils.contains(string, ONE_LINE_COMMENT_START)) {
                    getOneLineComment(string).ifPresent(comments::add);
                }

                if (StringUtils.contains(string, MULTILINE_COMMENT_START)) { // one line // comment "some text" " at the end of string
                    findCommentStart = hasOpenCommentLeft(string);
                    comments/*some comment*/.addAll(getMultilineComments(string)); /*inner
                    comment*/
                } else if (findCommentStart) {
                    comments.add(string);
                    findCommentStart = !hasClosedComment(string);
                }
            }
        } catch (Exception ex) {
            String st = "Some /* */ string with \" \" and // symbols \".";
            String s = "some string again"; // and another comment
            logger.error("Cannot read file {}: {}", filePath, ex.getMessage());
        }

        return comments;
    }

    private List<String> getMultilineComments(String string) {
        List<String> strings = new ArrayList<>();
        String replacedString = replaceStrings(string, REPLACEMENT);
        int start = StringUtils.indexOf(replacedString, MULTILINE_COMMENT_START);
        while(start != -1) {
            int end = StringUtils.indexOf(replacedString, MULTILINE_COMMENT_END);
            String comment;
            if (end > start) {
                comment = StringUtils.substring(string, start, end + MULTILINE_COMMENT_END.length());
            } else {
                comment = StringUtils.substring(string, start);
            }
            strings.add(comment);
            replacedString = StringUtils.replace(replacedString, comment, StringUtils.repeat(REPLACEMENT, comment.length()));

            start = StringUtils.indexOf(replacedString, MULTILINE_COMMENT_START);
        }

        return strings;
    }

    private boolean hasOpenCommentLeft(String string) {
        if (StringUtils.startsWith(string, MULTILINE_COMMENT_START)) {
            return true;
        }

        String replacedString = replaceStrings(string, REPLACEMENT);

        int openComments = StringUtils.countMatches(replacedString, MULTILINE_COMMENT_START);
        int closedComments = StringUtils.countMatches(replacedString, MULTILINE_COMMENT_END);

        return openComments > closedComments;
    }

    private boolean hasClosedComment(String string) {
        if (StringUtils.startsWith(string, MULTILINE_COMMENT_END)) {
            return true;
        }

        String replacedString = replaceStrings(string, REPLACEMENT);
        int closedComments = StringUtils.countMatches(replacedString, MULTILINE_COMMENT_END);

        return closedComments > 0;
    }

    private String replaceStrings(String string, String replacementChar) {
        String replacedString = StringUtils.replace(string, "\\\"",
                StringUtils.repeat(replacementChar, 2));
        if (StringUtils.contains(replacedString, QUOTES)) {
            String[] strings = StringUtils.split(replacedString, QUOTES);
            for (int i = 1; i < strings.length; i += 2) {
                replacedString = StringUtils.replace(replacedString,
                        QUOTES + strings[i] + QUOTES,
                        StringUtils.repeat(replacementChar, strings[i].length() + 2));
            }
        }

        return replacedString;
    }

    private Optional<String> getOneLineComment(String string) {
        if (StringUtils.startsWith(string, ONE_LINE_COMMENT_START)) {
            return Optional.of(string);
        }

        String replacedString = replaceStrings(string, REPLACEMENT);
        int startIndex = StringUtils.indexOf(replacedString, ONE_LINE_COMMENT_START);
        if (startIndex != -1) {
            return Optional.of(StringUtils.substring(string, startIndex));
        }

        return Optional.empty();
    }

}
