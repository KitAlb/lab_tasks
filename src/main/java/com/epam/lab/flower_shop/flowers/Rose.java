package com.epam.lab.flower_shop.flowers;

public class Rose implements Flower {
    @Override
    public double getPrice() {
        return 5;
    }
}
