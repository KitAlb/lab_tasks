package com.epam.lab.jdbc.service;

import com.epam.lab.jdbc.dao.implementation.WorksOnDaoImpl;
import com.epam.lab.jdbc.model.PK_WorksOn;
import com.epam.lab.jdbc.model.WorksOnEntity;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.List;

public class WorkOnService {
    public List<WorksOnEntity> findAll() throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        return new WorksOnDaoImpl().findAll();
    }

    public int create(WorksOnEntity entity) throws SQLException{
        return new WorksOnDaoImpl().create(entity);
    }

    public int update(WorksOnEntity entity) throws SQLException{
        return new WorksOnDaoImpl().update(entity);
    }

    public int delete(PK_WorksOn pk) throws SQLException{
        return new WorksOnDaoImpl().delete(pk);
    }
}
