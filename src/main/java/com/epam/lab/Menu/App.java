package com.epam.lab.Menu;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class App {

    private static Logger logger = LogManager.getLogger("ConsoleLogger");

    public static void main(String[] args) {
        Menu menu = new Menu(logger);
        Scanner scanner = new Scanner(System.in);
        Continue isContinue = Continue.YES;

        while (isContinue == Continue.YES) {
            logger.info("Do you want to make a choice? YES/NO");
            isContinue = Continue.of(scanner.next());

            int choiceIndex = menu.getContinue(isContinue).get();

            if (choiceIndex == 0) {
                return;
            }

            if (choiceIndex < 0 || choiceIndex > Choice.values().length) {
                logger.warn("Please choose right value");
            }

            menu.getChoice(Choice.values()[choiceIndex - 1]).run();
        }

    }


}
