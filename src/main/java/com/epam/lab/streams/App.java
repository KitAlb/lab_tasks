package com.epam.lab.streams;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class App {
    public static void main(String[] args) {
        //You can use another method (see below) for creating a list, had no time to refactor this fragment
        List<Integer> randomNumbers = createListGenerate();
        System.out.println(randomNumbers.toString());

        randomNumbers.stream()
                .max(Comparator.comparing(Integer::valueOf))
                .ifPresent(max -> System.out.println("Maximum found is " + max));

        randomNumbers.stream()
                .min(Comparator.comparing(Integer::valueOf))
                .ifPresent(min -> System.out.println("Minimum found is " + min));

        double average = randomNumbers.stream()
                .mapToInt(Integer::valueOf)
                .average().orElse(0);
        System.out.println("Average found is " + average);

        int sum1 = randomNumbers.stream()
                .mapToInt(Integer::valueOf)
                .sum();
        System.out.println("Sum found is " + sum1);

        Integer sum2 = randomNumbers.stream()
                .reduce(0, (a, b) -> a + b);
        System.out.println("Sum (reduce) found is " + sum2);

        System.out.println("Higher than average: " + randomNumbers.stream()
                .filter(number -> number > average)
                .count());
    }

    private static List<Integer> createListGenerate(){
        List<Integer> randomNumbers = new ArrayList<>();

        Stream<Integer> randomNumbersStream = Stream.generate( () -> (new Random()).nextInt(100) );
        randomNumbersStream.limit(10).forEach(randomNumbers::add);
        return randomNumbers;
    }

    private static List<Integer> createListInts(){
        return new Random().ints(0, 100)
                .limit(10)
                .boxed()
                .collect(Collectors.toList());
    }

    private static List<Integer> createListOf(){
        Random random = new Random();

        return Stream.of(random.nextInt(100),
                random.nextInt(100),
                random.nextInt(100))
                .collect(Collectors.toList());
    }
}
