package com.epam.lab.TextParser.parser;

import com.epam.lab.TextParser.parser.pojos.Punctuation;
import com.epam.lab.TextParser.parser.pojos.Sentence;
import com.epam.lab.TextParser.parser.pojos.Text;
import com.epam.lab.TextParser.parser.pojos.Word;
import org.apache.commons.lang3.RegExUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.epam.lab.TextParser.parser.pojos.Constants.PLACEHOLDER;
import static com.epam.lab.TextParser.parser.pojos.Constants.VOWELS;

public class TextParser {

    private static int SECOND_LETTER_INDEX = 1;

    private Text text;

    public TextParser(String text) {
        this.text = parse(text);
    }

    public long countSentencesWithDoubleWords() {
        return getText().countSentencesWithDoubleWords();
    }

    public List<String> getSentencesInAscendingOrder() {
        return getText().getSentencesInAscendingOrder();
    }

    public Optional<String> findFirstSentenceUniqueWord() {
        return getText().findUniqueWord(0);
    }

    public Set<String> getWordsBySizeInQuestions(int size) {
        return getText().getWordsBySizeInSentence(size, Punctuation.QMARK);
    }

    public List<String> getSentencesWithReplacesWords() {
        return getText().getSentences().stream()
                .map(this::getReplacedSentence)
                .collect(Collectors.toList());
    }

    public List<String> getWordsAlphabetically() {
        return getText().getSentences().stream()
                .flatMap(sentence -> sentence.getWords().stream()
                        .map(Word::getValue))
                .sorted(new Comparator<String>() {
                    @Override
                    public int compare(String o1, String o2) {
                        return StringUtils.compareIgnoreCase(o1, o2);
                    }
                })
                .collect(Collectors.toList());
    }

    public List<String> getWordsSortedByPercentage(String regex) {
        return getText().getSentences().stream()
                .flatMap(sentence -> sentence.getWords().stream())
                .sorted(new Comparator<Word>() {
                    @Override
                    public int compare(Word o1, Word o2) {
                        return Float.compare(o1.getPercentage(regex), o2.getPercentage(regex));
                    }
                })
                .map(Word::getValue)
                .collect(Collectors.toList());
    }

    public List<String> getWordsSortedBySecondLetter(String[] firstLetterFilter) {
        return getText().getSentences().stream()
                .flatMap(sentence -> sentence.getWords().stream())
                .filter(word -> word.isStartsWithAny(firstLetterFilter))
                .map(Word::getValue)
                .filter(word -> word.length() > 1)
                .sorted(new Comparator<String>() {
                    @Override
                    public int compare(String o1, String o2) {
                        return StringUtils.compareIgnoreCase(
                                StringUtils.substring(o1, SECOND_LETTER_INDEX, 2),
                                StringUtils.substring(o2, SECOND_LETTER_INDEX, 2)
                        );
                    }
                })
                .collect(Collectors.toList());
    }

    public List<String> getWordsSortedByLetterPercentage(String letter) {
        String regex = "[" + letter + "]";
        return getText().getSentences().stream()
                .flatMap(sentence -> sentence.getWords().stream())
                .sorted(new Comparator<Word>() {
                    @Override
                    public int compare(Word o1, Word o2) {
                        float o1Percentage = o1.getPercentage(regex);
                        float o2Percentage = o2.getPercentage(regex);
                        if (o1Percentage == o2Percentage) {
                            return StringUtils.compareIgnoreCase(o1.getValue(), o2.getValue());
                        }

                        return Float.compare(o1Percentage, o2Percentage);
                    }
                })
                .map(Word::getValue)
                .collect(Collectors.toList());
    }

    public Map<String, Map<Integer, Long>> getWordsCountBySentence(String[] words) {
        return Arrays.stream(words)
                .collect(Collectors.toMap(
                        w -> w,
                        this::getWordCountBySentence
                ));
    }

    private Map<Integer, Long> getWordCountBySentence(String word) {
        return IntStream.range(0, getText().getSentences().size())
                .boxed()
                .collect(Collectors.toMap(
                        i -> i + 1,
                        i -> getWordCountInSentence(i, word)
                ));
    }

    private Long getWordCountInSentence(Integer sentenceIndex, String word) {
        return getText().getSentences().get(sentenceIndex).getWordCount(word);
    }

    private String getReplacedSentence(Sentence sentence) {
        return sentence.getFirstWordStartsWithAny(VOWELS.split(""))
                .map(word ->
                        sentence.getLongestWord()
                                .map(longestWord -> swapWords(sentence.getValue(), word, longestWord))
                                .orElse(sentence.getValue())
                )
                .orElse(sentence.getValue());
    }

    private String swapWords(String sentence, String firstWord, String secondWord) {
        String result = RegExUtils.replaceFirst(sentence, firstWord, PLACEHOLDER);
        result = RegExUtils.replaceFirst(result, secondWord, firstWord);
        result = RegExUtils.replaceFirst(result, PLACEHOLDER, secondWord);

        return result;
    }

    private Text getText() {
        return this.text;
    }

    private Text parse(String value) {
        return new Text(value);
    }

}
