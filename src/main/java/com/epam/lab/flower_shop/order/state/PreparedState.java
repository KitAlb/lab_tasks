package com.epam.lab.flower_shop.order.state;

import com.epam.lab.flower_shop.order.Order;

public class PreparedState implements State {
    @Override
    public String deliver(Order order) {
        order.setState(new DeliveredState());
        return "Order was delivered.";
    }

    @Override
    public String cancel(Order order) {
        order.setState(new CanceledState());
        return "Order was cancelled. Do refund.";
    }
}
