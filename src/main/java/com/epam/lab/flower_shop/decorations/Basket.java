package com.epam.lab.flower_shop.decorations;

public class Basket implements Decoration {
    @Override
    public double getPrice() {
        return 10;
    }
}
