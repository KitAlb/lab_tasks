package com.epam.lab.arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;

public class App {
    private static Logger logger = LogManager.getLogger("ConsoleLogger");

    public static void main(String[] args) {
        try {
            int[] firstArray = Array.createArray();
            logger.info("First array: {}", Arrays.toString(firstArray));

            int[] secondArray = Array.createArray();
            logger.info("Second array: {}", Arrays.toString(secondArray));

            int[] thirdArray = Array.createArrayWithCommonNumbers(firstArray, secondArray);
            if (thirdArray.length > 0) {
                logger.info("Commons array: {}", Arrays.toString(thirdArray));
            } else {
                logger.error("No common items");
            }

            int[] fourthArray = Array.createArrayWithUniqueNumbers(firstArray, secondArray);
            if (fourthArray.length > 0) {
                logger.info("Uniques array: {}", Arrays.toString(fourthArray));
            } else {
                logger.error("Two arrays are completely similar");
            }

            int[] fifthArray = Array.deleteDuplicates(firstArray, 2);
            logger.info("Array without duplicates: {}", Arrays.toString(fifthArray));

            int[] sixthArray = Array.clearDuplicates(firstArray);
            logger.info("Cleared array: {}", Arrays.toString(sixthArray));
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
    }
}
