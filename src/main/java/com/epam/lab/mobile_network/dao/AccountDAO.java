package com.epam.lab.mobile_network.dao;

import com.epam.lab.mobile_network.model.Account;

import java.util.Optional;
import java.util.UUID;

public interface AccountDAO extends BaseDAO<Account, UUID> {
    Optional<Account> findByUserId(UUID userId);
}
