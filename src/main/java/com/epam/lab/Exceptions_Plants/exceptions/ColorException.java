package com.epam.lab.Exceptions_Plants.exceptions;

import com.epam.lab.Exceptions_Plants.enums.Color;

import java.util.Arrays;

public class ColorException extends RuntimeException {

    public ColorException(String colorString) {
        super("Unknown " + Color.class.getSimpleName() +
                " - " + colorString + ", allowed values are " + Arrays.toString(Color.values()));
    }
}
