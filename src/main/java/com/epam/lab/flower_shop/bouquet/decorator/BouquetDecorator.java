package com.epam.lab.flower_shop.bouquet.decorator;

import com.epam.lab.flower_shop.bouquet.Bouquet;
import com.epam.lab.flower_shop.decorations.Decoration;
import com.epam.lab.flower_shop.flowers.Flower;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class BouquetDecorator implements Bouquet {

    private Bouquet bouquet;
    private double additionalPrice = 0;
    private List<Flower> additionalFlowers = new ArrayList<>();
    private List<Decoration> additionalDecorations = new ArrayList<>();

    public BouquetDecorator(Bouquet bouquet) {
        this.bouquet = bouquet;
    }

    public void setAdditionalPrice(double additionalPrice) {
        this.additionalPrice = additionalPrice;
    }

    public void addFlowers(List<Flower> additionalFlowers) {
        this.additionalFlowers.addAll(additionalFlowers);
    }

    public void addFlower(Flower flower) {
        this.additionalFlowers.add(flower);
    }

    public void addDecorations(List<Decoration> additionalDecorations) {
        this.additionalDecorations.addAll(additionalDecorations);
    }

    public void addDecoration(Decoration decoration) {
        this.additionalDecorations.add(decoration);
    }

    @Override
    public double getPrice() {
        return bouquet.getPrice() +
                additionalFlowers.stream()
                        .map(Flower::getPrice)
                        .reduce((double) 0, Double::sum) +
                additionalDecorations.stream()
                        .map(Decoration::getPrice)
                        .reduce((double) 0, Double::sum) +
                additionalPrice;
    }

    @Override
    public List<Flower> getFlowers() {
        return Stream.concat(
                bouquet.getFlowers().stream(),
                additionalFlowers.stream())
                .collect(Collectors.toList());
    }

    @Override
    public List<Decoration> getDecorations() {
        return Stream.concat(
                bouquet.getDecorations().stream(),
                additionalDecorations.stream())
                .collect(Collectors.toList());
    }

}
