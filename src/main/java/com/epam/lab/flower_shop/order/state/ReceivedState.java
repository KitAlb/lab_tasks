package com.epam.lab.flower_shop.order.state;

import com.epam.lab.flower_shop.order.Order;

public class ReceivedState implements State {
    @Override
    public String pay(Order order) {
        order.setState(new PaidState());
        return "Order was paid.";
    }

    @Override
    public String cancel(Order order) {
        order.setState(new CanceledState());
        return "Order was canceled";
    }
}
