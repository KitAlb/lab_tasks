package com.epam.lab.pizza_store.store;

import com.epam.lab.pizza_store.pizza.Pizza;
import com.epam.lab.pizza_store.pizza.type.PizzaType;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Logger;

import java.util.Optional;

public abstract class PizzaStore {

    protected Logger logger;

    public PizzaStore(Logger logger) {
        this.logger = logger;
    }

    public Pizza orderPizza(PizzaType type) {
        return createPizza(type).map(pizza -> {
            pizza.prepare();
            pizza.bake();
            pizza.cut();
            pizza.box();

            return pizza;
        })
        .orElseThrow(() ->
                new IllegalArgumentException("We do not serve " + StringUtils.lowerCase(type.name()) + " pizza"));
    }

    protected abstract Optional<Pizza> createPizza(PizzaType type);

}
