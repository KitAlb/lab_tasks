package com.epam.lab.game.kalaha.wire.board;

import com.epam.lab.game.kalaha.wire.board.type.PitType;
import com.epam.lab.game.kalaha.wire.player.Player;

public abstract class Pit {

    private int seed = 0;
    private Player owner;
    private PitType type;

    public PitType getType() {
        return this.type;
    }

    public int getSeed() {
        return this.seed;
    }

    void setSeed(int seed) {
        this.seed = seed;
    }

    public int removeAllSeeds() {
        int seed = this.seed;
        this.seed = 0;
        return seed;
    }

    public void incrementSeed(int incrementWith) {
        this.seed += incrementWith;
    }

    public void incrementSeed() {
        this.incrementSeed(1);
    }

    public Player getOwner() {
        return owner;
    }

    void setOwner(Player owner) {
        this.owner = owner;
    }

    public boolean isEmpty() {
        return this.seed == 0;
    }

}
