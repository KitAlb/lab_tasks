package com.epam.lab.flower_shop.flowers;

public interface Flower {

    double getPrice();

}
