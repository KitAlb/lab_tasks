package com.epam.lab.Menu;

public enum Choice {

    CHOICE1 {
        @Override
        public String toString() {
            return "1. Choice 1";
        }
    },
    CHOICE2 {
        @Override
        public String toString() {
            return "2. Choice 2";
        }
    }

}
