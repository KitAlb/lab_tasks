package com.epam.lab.flower_shop.decorations;

public class Ribbon implements Decoration {
    @Override
    public double getPrice() {
        return 2;
    }
}
