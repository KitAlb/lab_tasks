package com.epam.lab.jdbc;

import com.epam.lab.jdbc.controller.MyView;

public class App {

    public static void main(String[] args) {
        new MyView().show();
    }

}
