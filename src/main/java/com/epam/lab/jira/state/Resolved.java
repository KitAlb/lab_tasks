package com.epam.lab.jira.state;

import com.epam.lab.jira.task.Task;

public class Resolved implements State {

    public void open(Task task) {
        task.setState(new Open());
        System.out.println("Reject resolution");
    }

    public void close(Task task) {
        task.setState(new Closed());
        System.out.println("Verify and close");
    }

    public void verify(Task task) {
        task.setState(new Verified());
        System.out.println("Verify");
    }

}
