package internationalization;

public enum Choice {
    CHOICE1 {
        @Override
        public String toString() {
            return "Choice 1";
        }
    },
    CHOICE2 {
        @Override
        public String toString() {
            return "Choice 2";
        }
    }
}
