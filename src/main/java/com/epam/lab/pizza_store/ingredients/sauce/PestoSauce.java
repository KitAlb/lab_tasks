package com.epam.lab.pizza_store.ingredients.sauce;

public class PestoSauce implements Sauce {
    @Override
    public String toString() {
        return "Pesto";
    }
}
