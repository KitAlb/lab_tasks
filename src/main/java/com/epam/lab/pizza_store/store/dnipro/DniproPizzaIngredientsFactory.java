package com.epam.lab.pizza_store.store.dnipro;

import com.epam.lab.pizza_store.ingredients.PizzaIngredientsFactory;
import com.epam.lab.pizza_store.ingredients.cheese.Cheese;
import com.epam.lab.pizza_store.ingredients.cheese.GranaPadano;
import com.epam.lab.pizza_store.ingredients.cheese.Mozzarella;
import com.epam.lab.pizza_store.ingredients.clams.Clams;
import com.epam.lab.pizza_store.ingredients.clams.SeaMix;
import com.epam.lab.pizza_store.ingredients.dough.Dough;
import com.epam.lab.pizza_store.ingredients.dough.ThickCrustDough;
import com.epam.lab.pizza_store.ingredients.pepperoni.Pepperoni;
import com.epam.lab.pizza_store.ingredients.pepperoni.SalamiPepperoni;
import com.epam.lab.pizza_store.ingredients.sauce.MarinaraSauce;
import com.epam.lab.pizza_store.ingredients.sauce.Sauce;
import com.epam.lab.pizza_store.ingredients.veggies.*;

import java.util.List;

public class DniproPizzaIngredientsFactory implements PizzaIngredientsFactory {
    @Override
    public Dough createDough() {
        return new ThickCrustDough();
    }

    @Override
    public Sauce createSauce() {
        return new MarinaraSauce();
    }

    @Override
    public List<Cheese> createCheese() {
        return List.of(
                new Mozzarella(),
                new GranaPadano()
        );
    }

    @Override
    public List<Veggies> createVeggies() {
        return List.of(
                new Onion(),
                new Eggplant(),
                new Mushrooms(),
                new SweetPepper()
        );
    }

    @Override
    public List<Pepperoni> createPepperoni() {
        return List.of(new SalamiPepperoni());
    }

    @Override
    public List<Clams> createClams() {
        return List.of(new SeaMix());
    }
}
