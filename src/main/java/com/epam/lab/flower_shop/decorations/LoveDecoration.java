package com.epam.lab.flower_shop.decorations;

public class LoveDecoration implements Decoration {
    @Override
    public double getPrice() {
        return 2;
    }
}
