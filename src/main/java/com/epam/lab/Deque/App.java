package com.epam.lab.Deque;

import com.epam.lab.Deque.deque.MyDeque;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class App {
    private static Logger logger = LogManager.getLogger("ConsoleLogger");

    public static void main(String[] args) {
        MyDeque deque = new MyDeque();
        deque.addFirst("Element 2 (Head)");
        deque.addLast("Element 3 (Tail)");
        logger.info(deque.removeLast());
        logger.info(deque.removeFirst());
    }
}
