package com.epam.lab.mobile_network.controllers;

@FunctionalInterface
public interface ControllerMethod {
    void run();
}
