package com.epam.lab.game.kalaha;

import com.epam.lab.game.kalaha.wire.board.Kalah;
import com.epam.lab.game.kalaha.wire.board.Pit;
import com.epam.lab.game.kalaha.wire.board.PlayerPit;
import com.epam.lab.game.kalaha.wire.board.type.PitType;
import com.epam.lab.game.kalaha.wire.player.Player;

import java.util.Arrays;
import java.util.List;

public class GameState {

    private List<Pit> pits;

    private Player currentPlayer;
    private Player playerA;
    private Player playerB;
    private Player firstPlayer;
    private boolean isExtraTurn = false;

    GameState(int seeds, Player playerA, Player playerB) {
        this.pits = Arrays.asList(
                new PlayerPit(seeds, playerA),
                new PlayerPit(seeds, playerA),
                new PlayerPit(seeds, playerA),
                new PlayerPit(seeds, playerA),
                new PlayerPit(seeds, playerA),
                new PlayerPit(seeds, playerA),
                new Kalah(playerA),
                new PlayerPit(seeds, playerB),
                new PlayerPit(seeds, playerB),
                new PlayerPit(seeds, playerB),
                new PlayerPit(seeds, playerB),
                new PlayerPit(seeds, playerB),
                new PlayerPit(seeds, playerB),
                new Kalah(playerB)
        );

        this.playerA = playerA;
        this.playerB = playerB;

        this.currentPlayer = playerA;
        this.firstPlayer = currentPlayer;
    }

    private boolean isDone() {
        return this.pits.stream()
                .filter(pit -> pit.getType() == PitType.PLAYER && pit.getOwner().equals(this.currentPlayer))
                .allMatch(Pit::isEmpty);
    }

    private boolean playerAHasWon() {
        return playerHasWon(playerA);
    }

    private boolean isDraw() {
        return this.getCurrentPlayersKalah().getSeed() == this.getOpponentsKalah().getSeed();
    }

    Player getWinner() throws Exception {
        if(this.isOngoing()) {
            throw new Exception("Game still running");
        }

        if(isDraw()) {
            return null;
        }

        return this.playerAHasWon() ?
                this.playerA :
                this.playerB;
    }

    private boolean playerHasWon(Player client) {
        return client.equals(this.currentPlayer) ?
                this.getCurrentPlayersKalah().getSeed() > this.getOpponentsKalah().getSeed() :
                this.getCurrentPlayersKalah().getSeed() < this.getOpponentsKalah().getSeed();
    }

    boolean isOngoing() {
        return !this.isDone();
    }

    private boolean isValidNum(int pitNum) {
        if(pitNum > 6 || pitNum < 1) {
            return false;
        }

        Pit chosenPit = getPit(pitNum);

        return !chosenPit.isEmpty();
    }

    GameState takeTurn() {
        return this.takeTurn(this.currentPlayer.takeTurn(this));
    }

    private GameState takeTurn(int pitNum) {
        this.isExtraTurn = false;

        if(!isValidNum(pitNum)) {
            return this;
        }

        Pit currentPit = getPit(pitNum);

        int seeds = currentPit.removeAllSeeds();
        while (seeds != 0) {
            currentPit = getNextPit(currentPit);
            currentPit.incrementSeed();
            --seeds;
        }

        if(this.isDone()) {
            this.takeOpponentSeed();
            return this;
        }

        if(currentPit.equals(getCurrentPlayersKalah())) {
            this.isExtraTurn = true;
            return this;
        }

        this.changeTurn();

        return this;
    }

    private Pit getPit(int pitNum) {
        return this.pits.get(
                this.firstPlayer.equals(this.currentPlayer) ?
                        pitNum - 1 :
                        pitNum + 6
        );
    }

    public boolean isExtraTurn() {
        return this.isExtraTurn;
    }

    private void changeTurn() {
        this.currentPlayer = this.playerA.equals(this.currentPlayer) ?
                this.playerB :
                this.playerA;
    }

    private Pit getNextPit(Pit currentPit) {
        Pit nextPit = this.pits.get((this.pits.indexOf(currentPit) + 1) % this.pits.size());

        if(nextPit.equals(this.getOpponentsKalah())) {
            nextPit = this.getNextPit(nextPit);
        }

        return nextPit;
    }

    private Kalah getCurrentPlayersKalah() {
        return getPlayerKalah(this.currentPlayer);
    }

    private Kalah getOpponentsKalah() {
        return this.getCurrentPlayersKalah().equals(getPlayerAKalah()) ? getPlayerBKalah() : getPlayerAKalah();
    }

    private Kalah getPlayerAKalah() {
        return this.getPlayerKalah(this.playerA);
    }

    private Kalah getPlayerBKalah() {
        return this.getPlayerKalah(this.playerB);
    }

    private Kalah getPlayerKalah(Player player) {
        return (Kalah) this.pits.stream()
                .filter(pit -> pit.getType() == PitType.KALAH && pit.getOwner().equals(player))
                .findFirst()
                .get();
    }

    private void takeOpponentSeed() {
        this.pits.stream()
                .filter(pit -> !pit.getOwner().equals(this.currentPlayer) && pit.getType() == PitType.PLAYER)
                .forEach(pit -> getCurrentPlayersKalah().incrementSeed(pit.removeAllSeeds()));
    }

    @Override
    public String toString() {
        String format = "{%2d}{%2d}{%2d}{%2d}{%2d}{%2d}\n{%2d}                {%2d}\n{%2d}{%2d}{%2d}{%2d}{%2d}{%2d}";

        if(firstPlayer.equals(currentPlayer)) {
            return String.format(
                    format,
                    this.pits.get(12).getSeed(),
                    this.pits.get(11).getSeed(),
                    this.pits.get(10).getSeed(),
                    this.pits.get(9).getSeed(),
                    this.pits.get(8).getSeed(),
                    this.pits.get(7).getSeed(),
                    this.pits.get(13).getSeed(),
                    this.pits.get(6).getSeed(),
                    this.pits.get(0).getSeed(),
                    this.pits.get(1).getSeed(),
                    this.pits.get(2).getSeed(),
                    this.pits.get(3).getSeed(),
                    this.pits.get(4).getSeed(),
                    this.pits.get(5).getSeed()
            );
        } else {
            return String.format(
                    format,
                    this.pits.get(5).getSeed(),
                    this.pits.get(4).getSeed(),
                    this.pits.get(3).getSeed(),
                    this.pits.get(2).getSeed(),
                    this.pits.get(1).getSeed(),
                    this.pits.get(0).getSeed(),
                    this.pits.get(6).getSeed(),
                    this.pits.get(13).getSeed(),
                    this.pits.get(7).getSeed(),
                    this.pits.get(8).getSeed(),
                    this.pits.get(9).getSeed(),
                    this.pits.get(10).getSeed(),
                    this.pits.get(11).getSeed(),
                    this.pits.get(12).getSeed()
            );
        }
    }

}
