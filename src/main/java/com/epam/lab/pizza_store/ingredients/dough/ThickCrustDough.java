package com.epam.lab.pizza_store.ingredients.dough;

public class ThickCrustDough implements Dough {
    @Override
    public String toString() {
        return "Thick Crust";
    }
}
