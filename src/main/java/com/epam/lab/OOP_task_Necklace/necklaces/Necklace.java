package com.epam.lab.OOP_task_Necklace.necklaces;

import com.epam.lab.OOP_task_Necklace.jewels.Jewel;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Necklace {

    private List<Jewel> jewels = new ArrayList<Jewel>();

    public Necklace() {
    }

    public Necklace(List<Jewel> jewels) {
        this.jewels.addAll(jewels);
    }

    public void add(Jewel jewel) {
        this.jewels.add(jewel);
    }

    public void addAll(List<Jewel> jewels) {
        this.jewels.addAll(jewels);
    }

    public void remove(Jewel jewel) {
        this.jewels.remove(jewel);
    }

    public void removeAll(List<Jewel> jewels) {
        this.jewels.removeAll(jewels);
    }

    public void clear() {
        this.jewels.clear();
    }

    public Integer getTotalWeight() {
        return this.jewels.stream()
                .mapToInt(Jewel::getWeight)
                .sum();
    }

    public Integer getTotalPrice() {
        return this.jewels.stream()
                .mapToInt(Jewel::getPrice)
                .sum();
    }

    public List<Jewel> getSortedByPrice() {
        return this.jewels.stream()
                .sorted(Comparator.comparingInt(Jewel::getPrice))
                .collect(Collectors.toList());
    }

    public List<Jewel> searchByTransparency(int min, int max) {
        return this.jewels.stream()
                .filter(jewel ->
                        jewel.getTransparency() >= min &&
                                jewel.getTransparency() <= max)
                .collect(Collectors.toList());
    }

}
