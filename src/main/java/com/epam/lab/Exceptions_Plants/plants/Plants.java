package com.epam.lab.Exceptions_Plants.plants;

import com.epam.lab.Exceptions_Plants.enums.Color;
import com.epam.lab.Exceptions_Plants.enums.Type;

public class Plants {
    private int size;
    private Color color;
    private Type type;

    public Plants(int size, Color color, Type type) {
        this.size = size;
        this.color = color;
        this.type = type;
    }

    @Override
    public String toString() {
        return "{Plants(size = " + this.size + "; " +
                "color = " + this.color + "; " +
                "type = " + type + ";)}";
    }
}
