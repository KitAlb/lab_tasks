package com.epam.lab.pizza_store;

import com.epam.lab.pizza_store.pizza.Pizza;
import com.epam.lab.pizza_store.pizza.type.PizzaType;
import com.epam.lab.pizza_store.store.PizzaStore;
import com.epam.lab.pizza_store.store.dnipro.DniproPizzaStore;
import com.epam.lab.pizza_store.store.kyiv.KyivPizzaStore;
import com.epam.lab.pizza_store.store.lviv.LvivPizzaStore;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class App {

    private static Logger logger = LogManager.getLogger("ConsoleLogger");

    public static void main(String[] args) {
        PizzaStore lvivStore = new LvivPizzaStore(logger);
        PizzaStore kyivStore = new KyivPizzaStore(logger);
        PizzaStore dniproStore = new DniproPizzaStore(logger);

        Pizza lvivPizza = lvivStore.orderPizza(PizzaType.PEPPERONI);
        logger.info("Ordered {} pizza", lvivPizza.getName());

        Pizza kyivPizza = kyivStore.orderPizza(PizzaType.CHEESE);
        logger.info("Ordered {} pizza", kyivPizza.getName());

        Pizza dniproPizza = dniproStore.orderPizza(PizzaType.VEGGIE);
        logger.info("Ordered {} pizza", dniproPizza.getName());
    }

}
