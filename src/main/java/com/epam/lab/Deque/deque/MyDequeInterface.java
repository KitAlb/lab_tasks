package com.epam.lab.Deque.deque;

public interface MyDequeInterface {
    void addFirst(Object o);
    void addLast(Object o);
    Object removeFirst();
    Object removeLast();
}
