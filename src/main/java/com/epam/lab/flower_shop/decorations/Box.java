package com.epam.lab.flower_shop.decorations;

public class Box implements Decoration {
    @Override
    public double getPrice() {
        return 7;
    }
}
