package com.epam.lab.pizza_store.ingredients;

import com.epam.lab.pizza_store.ingredients.cheese.Cheese;
import com.epam.lab.pizza_store.ingredients.clams.Clams;
import com.epam.lab.pizza_store.ingredients.dough.Dough;
import com.epam.lab.pizza_store.ingredients.pepperoni.Pepperoni;
import com.epam.lab.pizza_store.ingredients.sauce.Sauce;
import com.epam.lab.pizza_store.ingredients.veggies.Veggies;

import java.util.List;

public interface PizzaIngredientsFactory {

    Dough createDough();
    Sauce createSauce();
    List<Cheese> createCheese();
    List<Veggies> createVeggies();
    List<Pepperoni> createPepperoni();
    List<Clams> createClams();

}
