package com.epam.lab.Deque.deque;

import java.util.*;

public class MyDeque implements MyDequeInterface {
    private Object[] array;
    private int firstIndex;
    private int lastIndex;

    private static int INITIAL_ARRAY_SIZE = 10;
    private static int MAX_ARRAY_SIZE = 100;

    public MyDeque() {
        array = new Object[INITIAL_ARRAY_SIZE];
        firstIndex = 0;
        lastIndex = 9;
        // Initialize front index and rear index
    }

    @Override
    public void addFirst(Object o) {
        if (o == null)
            throw new NullPointerException();
        final Object[] es = array;
        es[firstIndex = dec(firstIndex, es.length)] = o;
        if (firstIndex == lastIndex)
            grow(1);
    }

    @Override
    public void addLast(Object o) {
        if (o == null)
            throw new NullPointerException();
        final Object[] es = array;
        es[lastIndex] = o;
        if (firstIndex == (lastIndex = inc(lastIndex, es.length)))
            grow(1);
    }

    @Override
    public Object removeFirst() {
        final Object[] es = array;
        final int f = firstIndex;
        Object o = es[f];
        if (o != null) {
            es[f] = null;
            firstIndex = inc(f, es.length);
        }

        if (o == null) {
            throw new NoSuchElementException();
        }

        return o;
    }

    @Override
    public Object removeLast() {
        final Object[] es = array;
        final int l = dec(lastIndex, es.length);
        Object o = es[l];
        if (o != null)
            es[lastIndex = l] = null;

        if (o == null) {
            throw new NoSuchElementException();
        }

        return o;
    }

    private static int inc(int i, int modulus) {
        if (++i >= modulus) i = 0;
        return i;
    }

    private static int dec(int i, int modulus) {
        if (--i < 0) i = modulus - 1;
        return i;
    }

    private void grow(int needed) {
        final int oldCapacity = array.length;
        int newCapacity;

        int jump = (oldCapacity < 64) ? (oldCapacity + 2) : (oldCapacity >> 1);
        if (jump < needed || (newCapacity = (oldCapacity + jump)) - MAX_ARRAY_SIZE > 0)
            newCapacity = newCapacity(needed, jump);
        final Object[] es = array = Arrays.copyOf(array, newCapacity);
        // Exceptionally, here tail == head needs to be disambiguated
        if (lastIndex < firstIndex || (lastIndex == firstIndex && es[firstIndex] != null)) {
            // wrap around; slide first leg forward to end of array
            int newSpace = newCapacity - oldCapacity;
            System.arraycopy(es, firstIndex,
                    es, firstIndex + newSpace,
                    oldCapacity - firstIndex);
            for (int i = firstIndex, to = (firstIndex += newSpace); i < to; i++)
                es[i] = null;
        }
    }

    private int newCapacity(int needed, int jump) {
        final int oldCapacity = array.length, minCapacity;
        if ((minCapacity = oldCapacity + needed) - MAX_ARRAY_SIZE > 0) {
            if (minCapacity < 0)
                throw new IllegalStateException("Sorry, deque too big");
            return Integer.MAX_VALUE;
        }
        if (needed > jump)
            return minCapacity;
        return (oldCapacity + jump - MAX_ARRAY_SIZE < 0)
                ? oldCapacity + jump
                : MAX_ARRAY_SIZE;
    }
}
