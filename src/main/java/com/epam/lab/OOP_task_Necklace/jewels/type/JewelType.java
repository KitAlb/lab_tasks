package com.epam.lab.OOP_task_Necklace.jewels.type;

public enum JewelType {
    Opal,
    Ruby,
    Sapphire
}
