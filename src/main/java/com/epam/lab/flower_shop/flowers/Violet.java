package com.epam.lab.flower_shop.flowers;

public class Violet implements Flower {
    @Override
    public double getPrice() {
        return 1;
    }
}
