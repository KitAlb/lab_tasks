package com.epam.lab.pizza_store.ingredients.cheese;

public class Feta implements Cheese {
    @Override
    public String toString() {
        return "Feta";
    }
}
