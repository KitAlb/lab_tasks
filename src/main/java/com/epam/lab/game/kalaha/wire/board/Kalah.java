package com.epam.lab.game.kalaha.wire.board;

import com.epam.lab.game.kalaha.wire.board.type.PitType;
import com.epam.lab.game.kalaha.wire.player.Player;

public class Kalah extends Pit {

    private PitType type = PitType.KALAH;

    public Kalah(Player owner) {
        this.setOwner(owner);
    }

    @Override
    public PitType getType() {
        return this.type;
    }

    @Override
    public String toString() {
        return "Kalah{" +
                "seed=" + this.getSeed() +
                ", owner=" + this.getOwner() +
                '}';
    }

}
