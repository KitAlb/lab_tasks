package com.epam.lab.Exceptions_Plants;

import com.epam.lab.Exceptions_Plants.enums.Color;
import com.epam.lab.Exceptions_Plants.enums.Type;
import com.epam.lab.Exceptions_Plants.exceptions.ColorException;
import com.epam.lab.Exceptions_Plants.exceptions.TypeException;
import com.epam.lab.Exceptions_Plants.plants.Plants;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Plants[] plants = new Plants[5];
        System.out.println("Please enter size, color and type:");
        Scanner sc = new Scanner(System.in);
        for (int i = 0; i < plants.length; i++) {
            int size = sc.nextInt();
            String color = sc.next();
            String type = sc.next();
            try {
                Plants plant = new Plants(size, Color.of(color), Type.of(type));
                System.out.println("Created plant: " + plant);
            } catch (ColorException e) {
                System.out.println("Wrong color: " + e.getMessage());
            } catch (TypeException e) {
                System.out.println("Wrong type: " + e.getMessage());
            }
        }
    }
}
