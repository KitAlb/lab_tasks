package com.epam.lab.hero_challenge;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class App {
    private static Logger logger = LogManager.getLogger("ConsoleLogger");

    public static void main(String[] args) {
        Game game = new Game(logger);
        game.run();
    }
}
