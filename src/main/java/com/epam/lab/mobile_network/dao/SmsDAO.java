package com.epam.lab.mobile_network.dao;

import com.epam.lab.mobile_network.model.Sms;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface SmsDAO extends BaseDAO<Sms, UUID> {
    Optional<List<Sms>> findByReceiverId(UUID receiverId);
    Optional<List<Sms>> findBySenderId(UUID senderId);
    Optional<List<Sms>> findBySenderAndReceiverId(UUID senderId, UUID receiverId);
}
