package com.epam.lab.flower_shop.bouquet.implementation;

import com.epam.lab.flower_shop.bouquet.Bouquet;
import com.epam.lab.flower_shop.decorations.Decoration;
import com.epam.lab.flower_shop.flowers.Flower;

import java.util.ArrayList;
import java.util.List;

public class CustomBouquet implements Bouquet {
    private List<Flower> flowers;
    private List<Decoration> decorations;

    public CustomBouquet() {
        flowers = new ArrayList<>();
        decorations = new ArrayList<>();
    }

    @Override
    public List<Flower> getFlowers() {
        return flowers;
    }

    @Override
    public List<Decoration> getDecorations() {
        return decorations;
    }
}
