package com.epam.lab.game.kalaha.wire.board;

import com.epam.lab.game.kalaha.wire.board.type.PitType;
import com.epam.lab.game.kalaha.wire.player.Player;

public class PlayerPit extends Pit {

    private PitType type = PitType.PLAYER;

    public PlayerPit(int seed, Player owner) {
        this.setSeed(seed);
        this.setOwner(owner);
    }

    @Override
    public PitType getType() {
        return this.type;
    }

}
