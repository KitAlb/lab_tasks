package com.epam.lab.Logger_Task.loggers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LoggerWarn implements MyLogger {

    private static final Logger logger = LogManager.getLogger(LoggerWarn.class);

    @Override
    public void log() {
        logger.trace("LoggerWarn: trace");
        logger.debug("LoggerWarn: debug");
        logger.info("LoggerWarn: info");
        logger.warn("LoggerWarn: warn");
        logger.error("LoggerWarn: error");
    }
}
