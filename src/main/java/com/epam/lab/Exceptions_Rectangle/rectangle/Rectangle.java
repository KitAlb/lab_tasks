package com.epam.lab.Exceptions_Rectangle.rectangle;

public class Rectangle {

    public static int squareRectangle(int a, int b) {
        if (a <= 0 || b <= 0) {
            throw new IllegalArgumentException("Illegal argument, input values should be positive.");
        } else {
            return a * b;
        }
    }

}
