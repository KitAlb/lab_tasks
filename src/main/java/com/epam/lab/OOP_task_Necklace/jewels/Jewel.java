package com.epam.lab.OOP_task_Necklace.jewels;

public abstract class Jewel {

    private int pricePerKar;
    private int weight;
    private int transparency;

    private Jewel() {
    }

    Jewel(int pricePerKar, int weight, int transparency) {
        this.pricePerKar = pricePerKar;
        this.weight = weight;
        this.transparency = transparency;
    }

    public int getPricePerKar() {
        return pricePerKar;
    }

    public int getWeight() {
        return weight;
    }

    public int getTransparency() {
        return transparency;
    }

    public int getPrice() {
        return weight * pricePerKar;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() +
                ", Price per Karat = " + pricePerKar + " USD" +
                ", Weight = " + weight + " kar" +
                ", Total Price = " + getPrice() + " USD" +
                ", Transparency = " + transparency + "%";
    }

}
