package com.epam.lab.functional_interface;

public interface MyFunctionalInterface {
    public int myMethod(int a, int b, int c);
}
