package com.epam.lab.jira;

import com.epam.lab.jira.task.Task;
import org.apache.commons.lang3.StringUtils;

import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String keyMenu;
        Task task = new Task();
        do {
            System.out.println();
            System.out.println(StringUtils.repeat("-", 30));
            System.out.println("   1 - Open   " + "   2 - Progress");
            System.out.println("   3 - Review " + "   4 - Resolve");
            System.out.println("   5 - Verify " + "   6 - Close");
            System.out.println("   7 - Block  " + "   Q - Exit");
            System.out.println("   Please, select menu point.");

            keyMenu = input.nextLine().toUpperCase();
            try {
                switch (keyMenu) {
                    case "1":
                        task.open();
                        break;
                    case "2":
                        task.progress();
                        break;
                    case "3":
                        task.review();
                        break;
                    case "4":
                        task.resolve();
                        break;
                    case "5":
                        task.verify();
                        break;
                    case "6":
                        task.close();
                        break;
                    case "7":
                        task.block();
                        break;
                }
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
