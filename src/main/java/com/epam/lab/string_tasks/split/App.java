package com.epam.lab.string_tasks.split;

import java.util.Arrays;

public class App {
    public static void main(String[] args) {
        String string1 = "uyweyr the uyeuur you euruiei the euru";
        System.out.println(string1);
        System.out.println(
                Arrays.toString(
                        Arrays.stream(string1.split("the"))
                                .flatMap(string -> Arrays.stream(string.split("you")))
                                .toArray()
                )
        );
    }
}
