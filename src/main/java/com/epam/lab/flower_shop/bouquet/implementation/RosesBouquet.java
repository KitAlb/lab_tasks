package com.epam.lab.flower_shop.bouquet.implementation;

import com.epam.lab.flower_shop.bouquet.Bouquet;
import com.epam.lab.flower_shop.decorations.Decoration;
import com.epam.lab.flower_shop.decorations.Greenery;
import com.epam.lab.flower_shop.flowers.Flower;
import com.epam.lab.flower_shop.flowers.Rose;

import java.util.Collections;
import java.util.List;

public class RosesBouquet implements Bouquet {
    private List<Flower> flowers;
    private List<Decoration> decorations;

    public RosesBouquet(int quantity) {
        flowers = Collections.nCopies(quantity, new Rose());
        decorations = List.of(new Greenery());
    }

    @Override
    public List<Flower> getFlowers() {
        return flowers;
    }

    @Override
    public List<Decoration> getDecorations() {
        return decorations;
    }
}
