package com.epam.lab.lambda_command;

import java.util.ArrayList;
import java.util.List;

class Invoker {
    private final List<Action> actions = new ArrayList<>();

    String executeOperation(Action action) {
        actions.add(action);

        return action.perform();
    }
}
