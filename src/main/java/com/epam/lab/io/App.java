package com.epam.lab.io;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class App {

    private static Logger logger = LogManager.getLogger("ConsoleLogger");

    public static void main(String[] args) {
        FileReader fileReader = new FileReader(logger);

//        fileReader.readFileDefault();
//        fileReader.readFileBuffered(10);
//        fileReader.readFileBuffered(100);

        fileReader.getComments("src/main/java/com/epam/lab/io/FileReader.java")
                .forEach(comment -> logger.info(comment));
    }

}
