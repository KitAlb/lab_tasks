package com.epam.lab.game.kalaha;

import com.epam.lab.game.kalaha.wire.player.Player;

public class Game {

    private final int seeds;
    private Player playerA;
    private Player playerB;
    private GameState winnerState;

    public Game(Player playerA, Player playerB, int seeds) {
        this.playerA = playerA;
        this.playerB = playerB;
        this.seeds = seeds;
    }

    private GameState initializeState() {
        return new GameState(this.seeds, playerA, playerB);
    }

    public void start() {

        GameState currentState = initializeState();

        while (currentState.isOngoing()) {
            currentState = currentState.takeTurn();
        }

        this.winnerState = currentState;
    }

    public Player getWinner() throws Exception {
        return this.winnerState.getWinner();
    }

}
