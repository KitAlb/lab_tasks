package com.epam.lab.mobile_network.connection;

import com.epam.lab.mobile_network.configuration.PropertiesLoader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Optional;
import java.util.Properties;

import static com.epam.lab.mobile_network.utils.Constants.*;

public class ConnectionManager {
    private static Logger logger = LogManager.getLogger("ConsoleLogger");
    private static Connection connection = null;

    private ConnectionManager() {
    }

    public static Optional<Connection> getConnection() {
        if (connection == null) {
            Properties properties = PropertiesLoader.loadProperties();
            String url = properties.getProperty(DB_CONNECTION);
            String user = properties.getProperty(DB_USER);
            String password = properties.getProperty(DB_PASSWORD);

            try {
                connection = DriverManager.getConnection(url, user, password);
            } catch (SQLException ex) {
                logger.error("SQLException: " + ex.getMessage());
            }
        }
        return Optional.ofNullable(connection);
    }
}
