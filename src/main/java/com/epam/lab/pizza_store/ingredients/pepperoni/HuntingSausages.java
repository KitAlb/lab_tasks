package com.epam.lab.pizza_store.ingredients.pepperoni;

public class HuntingSausages implements Pepperoni {
    @Override
    public String toString() {
        return "Hunting Sausages";
    }
}
