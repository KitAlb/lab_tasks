package com.epam.lab.flower_shop.flowers;

public class Tulip implements Flower {
    @Override
    public double getPrice() {
        return 3;
    }
}
