package com.epam.lab.TextParser.controller;

@FunctionalInterface
public interface ControllerMethod {
    void run();
}
