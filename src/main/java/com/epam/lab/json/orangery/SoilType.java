package com.epam.lab.json.orangery;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum SoilType {
    @JsonProperty("loam")
    LOAM,
    @JsonProperty("silt")
    SILT,
    @JsonProperty("clay")
    CLAY,
    @JsonProperty("sand")
    SAND
}
