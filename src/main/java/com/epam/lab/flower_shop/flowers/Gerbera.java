package com.epam.lab.flower_shop.flowers;

public class Gerbera implements Flower {
    @Override
    public double getPrice() {
        return 3;
    }
}
