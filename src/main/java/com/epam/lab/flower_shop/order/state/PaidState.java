package com.epam.lab.flower_shop.order.state;

import com.epam.lab.flower_shop.order.Order;

public class PaidState implements State {
    @Override
    public String prepare(Order order) {
        order.setState(new PreparedState());
        return "Order was prepared.";
    }

    @Override
    public String cancel(Order order) {
        order.setState(new CanceledState());
        return "Order was cancelled. Do refund.";
    }
}
