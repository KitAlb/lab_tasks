package com.epam.lab.pizza_store.ingredients.sauce;

public class PlumTomatoSauce implements Sauce {
    @Override
    public String toString() {
        return "Plum Tomato";
    }
}
