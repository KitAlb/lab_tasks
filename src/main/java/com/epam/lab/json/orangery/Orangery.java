package com.epam.lab.json.orangery;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

public class Orangery {

    private List<Flower> flowers = new ArrayList<>();

    public List<Flower> getFlowers() {
        return flowers;
    }

    public void setFlowers(List<Flower> flowers) {
        this.flowers = flowers;
    }

    @Override
    public String toString() {
        StringJoiner joiner = new StringJoiner(",\n", "[\n", "\n]");
        flowers.forEach(flower -> joiner.add(flower.toString()));
        return "Orangery{" +
                "flowers=" + joiner.toString() +
                "}";
    }
}
