package com.epam.lab.Menu;

import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.function.Supplier;

public class Menu {

    private Map<Choice, Runnable> choices = new HashMap<>();
    private Map<Continue, Supplier<Integer>> toContinue = new HashMap<>();
    private Logger logger;

    Menu(Logger logger) {
        this.logger = logger;
        toContinue.put(Continue.YES, this::choiceYes);
        toContinue.put(Continue.NO, this::choiceNo);
        choices.put(Choice.CHOICE1, this::choiceMethod1);
        choices.put(Choice.CHOICE2, this::choiceMethod2);
    }

    private Integer choiceYes() {
        Scanner scanner = new Scanner(System.in);
        for (Choice value : Choice.values()) {
            System.out.println(value);
        }
        logger.info("Make your choice:");
        return scanner.nextInt();
    }

    private Integer choiceNo() {
        logger.info("Okay");

        return 0;
    }

    private void choiceMethod1() {
        logger.info("You've chosen 1");
    }

    private void choiceMethod2() {
        logger.info("You've chosen 2");
    }

    public Runnable getChoice(Choice choice) {
        return choices.get(choice);
    }

    public Supplier<Integer> getContinue(Continue choice) {
        return toContinue.get(choice);
    }
    
}
