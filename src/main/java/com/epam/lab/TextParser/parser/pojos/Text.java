package com.epam.lab.TextParser.parser.pojos;

import org.apache.commons.collections4.CollectionUtils;

import java.text.BreakIterator;
import java.util.*;
import java.util.stream.Collectors;

public class Text {

    private List<Sentence> sentences;
    private String value;

    public Text(String value) {
        this.value = normalizeSpace(value);
    }

    public long countSentencesWithDoubleWords() {
        return getSentences().stream()
                .filter(Sentence::isContainDoubles)
                .count();
    }

    public Optional<String> findUniqueWord(int sentenceNumber) {
        if (sentenceNumber >= getSentences().size()) {
            throw new IllegalArgumentException("Sentence number is out of range.");
        }

        for (Word word : getSentences().get(sentenceNumber).getWords()) {
            boolean isContains = false;
            for (int i = 0; i < getSentences().size(); i++) {
                if (i == sentenceNumber) {
                    continue;
                }
                if (getSentences().get(i).isContainWord(word)) {
                    isContains = true;
                    break;
                }
            }
            if (!isContains) {
                return Optional.of(word.getValue());
            }
        }

        return Optional.empty();
    }

    public List<Sentence> getSentences() {
        if (CollectionUtils.isEmpty(this.sentences)) {
            this.sentences = parse(this.value);
        }

        return this.sentences;
    }

    public List<String> getSentencesInAscendingOrder() {
        return getSentences().stream()
                .sorted(new Comparator<Sentence>() {
                    @Override
                    public int compare(Sentence o1, Sentence o2) {
                        return Integer.compare(o1.getWords().size(), o2.getWords().size());
                    }
                }).map(Sentence::getValue)
                .collect(Collectors.toList());
    }

    public Set<String> getWordsBySizeInSentence(int size, Punctuation punctuation) {
        return getSentences().stream()
                .filter(sentence -> sentence.isEndsWith(punctuation))
                .flatMap(sentence -> sentence.getWordsBySize(size).stream())
                .collect(Collectors.toSet());
    }

    private List<Sentence> parse(String value) {
        List<Sentence> sentences = new ArrayList<>();

        BreakIterator breakIterator = BreakIterator.getSentenceInstance(Locale.US);
        breakIterator.setText(value);
        int index = 0;
        while (breakIterator.next() != BreakIterator.DONE) {
            String sentence = value.substring(index, breakIterator.current());
            sentences.add(new Sentence(sentence));
            index = breakIterator.current();
        }
        return sentences;
    }

    private String normalizeSpace(String value) {
        return value.replaceAll("\\s+", " ");
    }

}
