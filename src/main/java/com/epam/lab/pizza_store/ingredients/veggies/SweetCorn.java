package com.epam.lab.pizza_store.ingredients.veggies;

public class SweetCorn implements Veggies {
    @Override
    public String toString() {
        return "Sweet Corn";
    }
}
