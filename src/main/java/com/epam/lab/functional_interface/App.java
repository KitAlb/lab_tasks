package com.epam.lab.functional_interface;

public class App {
    public static void main(String[] args) {
        MyFunctionalInterface lambdaSum = (a, b, c) -> a + b + c;
        System.out.println("Sum of 10, 20, 30 = " + lambdaSum.myMethod(10, 20, 30));
        MyFunctionalInterface lambdaAverage = (a, b, c) -> (a + b + c) / 3;
        System.out.println("Average of 10, 20, 30 = " + lambdaAverage.myMethod(10, 20, 30));
    }
}
