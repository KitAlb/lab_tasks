package com.epam.lab.folders_content;

public enum FileType {

    FILE("file"),
    DIRECTORY("directory");

    private String value;

    FileType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }

}
