package com.epam.lab.pizza_store.store.kyiv;

import com.epam.lab.pizza_store.ingredients.PizzaIngredientsFactory;
import com.epam.lab.pizza_store.ingredients.cheese.Cheese;
import com.epam.lab.pizza_store.ingredients.cheese.Feta;
import com.epam.lab.pizza_store.ingredients.cheese.Mozzarella;
import com.epam.lab.pizza_store.ingredients.clams.Clams;
import com.epam.lab.pizza_store.ingredients.clams.PacificRazorClams;
import com.epam.lab.pizza_store.ingredients.clams.SeaMix;
import com.epam.lab.pizza_store.ingredients.dough.Dough;
import com.epam.lab.pizza_store.ingredients.dough.ThinCrustDough;
import com.epam.lab.pizza_store.ingredients.pepperoni.Chorizo;
import com.epam.lab.pizza_store.ingredients.pepperoni.HuntingSausages;
import com.epam.lab.pizza_store.ingredients.pepperoni.Pepperoni;
import com.epam.lab.pizza_store.ingredients.sauce.PlumTomatoSauce;
import com.epam.lab.pizza_store.ingredients.sauce.Sauce;
import com.epam.lab.pizza_store.ingredients.veggies.*;

import java.util.List;

public class KyivPizzaIngredientsFactory implements PizzaIngredientsFactory {
    @Override
    public Dough createDough() {
        return new ThinCrustDough();
    }

    @Override
    public Sauce createSauce() {
        return new PlumTomatoSauce();
    }

    @Override
    public List<Cheese> createCheese() {
        return List.of(new Mozzarella(), new Feta());
    }

    @Override
    public List<Veggies> createVeggies() {
        return List.of(
                new Tomato(),
                new Eggplant(),
                new Onion(),
                new SweetPepper(),
                new ChiliPepper()
        );
    }

    @Override
    public List<Pepperoni> createPepperoni() {
        return List.of(new Chorizo(), new HuntingSausages());
    }

    @Override
    public List<Clams> createClams() {
        return List.of(new SeaMix(), new PacificRazorClams());
    }
}
