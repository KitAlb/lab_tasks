package com.epam.lab.pizza_store.ingredients.pepperoni;

public class Chorizo implements Pepperoni {
    @Override
    public String toString() {
        return "Chorizo";
    }
}
