package com.epam.lab.stream_words;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class App {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        List<String> strings = new ArrayList<>();

        String s;

        do {
            s = in.nextLine();
            if (!s.isBlank()) {
                strings.addAll(Arrays.asList(s.split(" ")));
            }
        } while (!s.isBlank());

        List<String> uniques = getUniqueWords(strings);

        System.out.println("Unique words count: " + uniques.size());
        System.out.println("Unique words sorted: " + uniques.stream()
                .sorted()
                .collect(Collectors.toList())
                .toString());
        System.out.println("Words count: " + strings.stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting())));

        System.out.println("Chars count: " + strings.stream().map(string ->
                string.chars()
                        .mapToObj(i->(char)i)
                        .filter(Character::isLowerCase)
                        .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
        )
                .map(Map::entrySet)
                .flatMap(Collection::stream)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, Long::sum)).toString());
    }

    private static List<String> getUniqueWords(List<String> strings) {
        List<String> uniques = new ArrayList<>();

        for (String word : strings) {
            if (!uniques.contains(word)) {
                uniques.add(word);
            }
        }

        return uniques;
    }

}
