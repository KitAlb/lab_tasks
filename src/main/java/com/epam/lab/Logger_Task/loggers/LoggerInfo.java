package com.epam.lab.Logger_Task.loggers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LoggerInfo implements MyLogger {

    private static final Logger logger = LogManager.getLogger(LoggerInfo.class);

    @Override
    public void log() {
        logger.trace("LoggerInfo: trace");
        logger.debug("LoggerInfo: debug");
        logger.info("LoggerInfo: info");
        logger.warn("LoggerInfo: warn");
        logger.error("LoggerInfo: error");
    }
}
