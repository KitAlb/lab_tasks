package com.epam.lab.hero_challenge;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.Random;
import java.util.StringJoiner;

public class Game {
    private Logger logger;
    private static int DOORS_COUNT = 10;

    public Game(Logger logger) {
        this();
        this.logger = logger;
    }

    public Game() {
    }

    public void run() {
        Hero hero = new Hero();
        Door[] setOfDoors = generateRandomDoors(DOORS_COUNT);
        logger.info("Recommended order is: " + Arrays.toString(getRecommendedOrder(setOfDoors)));

        for (Door door : setOfDoors) {
            logger.info("Current hero strength is " + hero.getStrength() + ".");
            logger.info("Death awaits in " + countDeathPossibility(hero.getStrength(), setOfDoors) + " doors.");
            showDoors(setOfDoors, door);
            if (openDoor(hero, door)) {
                break;
            }
        }

        logger.info("Hero final Strength was " + hero.getStrength() + ".");
    }

    private int[] getRecommendedOrder(Door[] doors) {
        int[] order = new int[DOORS_COUNT];

        int index = 0;
        for (Door door : getDoorsByType(doors, DoorType.ARTIFACT)) {
            order[index] = door.getNumber();
            index++;
        }

        for (Door door : getMonsterDoorsSorted(doors)) {
            order[index] = door.getNumber();
            index++;
        }

        return order;
    }

    private Door[] getMonsterDoorsSorted(Door[] doors) {
        return sortDoorsByStrength(getDoorsByType(doors, DoorType.MONSTER));
    }

    private Door[] sortDoorsByStrength(Door[] doors) {
        Door temp;

        for (int i = 1; i < doors.length; i++) {
            for (int j = i; j > 0; j--) {
                if (doors[j].getStrength() < doors[j - 1].getStrength()) {
                    temp = doors[j];
                    doors[j] = doors[j - 1];
                    doors[j - 1] = temp;
                }
            }
        }

        return doors;
    }

    private Door[] getDoorsByType(Door[] doors, DoorType type) {
        Door[] filteredDoors = new Door[countDoors(doors, type)];

        int index = 0;
        for (Door door : doors) {
            if (door.getType() == type) {
                filteredDoors[index] = door;
                index++;
            }
        }

        return filteredDoors;
    }

    private int countDoors(Door[] doors, DoorType type) {
        int count = 0;
        for (Door door : doors) {
            if (door.getType() == type) {
                count++;
            }
        }

        return count;
    }

    private int countDeathPossibility(int heroStrength, Door[] doors) {
        return checkDoor(heroStrength, 0, doors);
    }

    private int checkDoor(int heroStrength, int counter, Door[] doors) {
        int deathCounter = 0;
        if (counter < DOORS_COUNT - 1) {
            deathCounter += checkDoor(heroStrength, counter + 1, doors);
        }

        return doors[counter].getType() == DoorType.MONSTER && heroStrength < doors[counter].getStrength() ?
                deathCounter + 1 :
                deathCounter;
    }

    private boolean openDoor(Hero hero, Door door){
        if (door.getType() == DoorType.MONSTER) {
            return !killMonster(hero.getStrength(), door.visit());
        } else {
            hero.increaseStrength(door.visit());
            logger.info("Hero has found an artifact with a strength of " + door.getStrength() + "! " +
                    "His strength now is "+ hero.getStrength());
            return false;
        }
    }

    private boolean killMonster(int heroStrength, int monsterStrength) {
        if (monsterStrength > heroStrength) {
            logger.info("The strength of monster was " + monsterStrength + "! Hero is defeated.");
            return false;
        } else {
            logger.info("The strength of monster was " + monsterStrength + "! Hero has won.");
            return true;
        }
    }

    private Door[] generateRandomDoors(int count) {
        Door[] setOfDoors = new Door[count];
        for (int i = 0; i < count; i++) {
            DoorType type = randomType();
            int strength = randomStrength(type);
            setOfDoors[i] = new Door(i + 1, type, strength);
        }

        return setOfDoors;
    }

    private DoorType randomType() {
        int pick = new Random().nextInt(DoorType.values().length);
        return DoorType.values()[pick];
    }

    private int randomStrength(DoorType type){
        if (type == DoorType.MONSTER){
            return new Random().nextInt(95)+5;
        }
        if (type==DoorType.ARTIFACT){
            return new Random().nextInt(70)+10;
        } else return 0;
    }

    private void showDoors(Door[] doors, Door selectedDoor) {
        logger.debug(getBorder());
        logger.debug(getHeader());
        for (Door door : doors) {
            logger.debug(getRow(door, selectedDoor));
        }
        logger.debug(getBorder());
    }

    private String getBorder() {
        return StringUtils.repeat("-", 45);
    }

    private String getHeader() {
        return getRowString(new String[] {"Door #", "Type", "Strength", "Visited"});
    }

    private String getRow(Door tableDoor, Door selectedDoor) {
        return tableDoor.getNumber() == selectedDoor.getNumber() ?
                getRow(tableDoor) + " <--- Hero" :
                getRow(tableDoor);
    }

    private String getRow(Door door) {
        return getRowString(new String[] {
                "Door " + door.getNumber(),
                door.getType().toString(),
                String.valueOf(door.getStrength()),
                String.valueOf(door.isVisited())
        });
    }

    private String getRowString(String[] items) {
        StringJoiner joiner = new StringJoiner("|", "|", "|");
        for (String item : items) {
            joiner.add(StringUtils.center(item, 10, " "));
        }
        return joiner.toString();
    }
}
