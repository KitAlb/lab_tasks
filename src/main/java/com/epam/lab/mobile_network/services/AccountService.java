package com.epam.lab.mobile_network.services;

import com.epam.lab.mobile_network.dao.AccountDAO;
import com.epam.lab.mobile_network.dao.implementation.AccountDaoImpl;
import com.epam.lab.mobile_network.model.Account;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class AccountService {
    private static Logger logger = LogManager.getLogger("ConsoleLogger");
    private AccountDAO dao = new AccountDaoImpl();

    public Optional<List<Account>> findAll() {
        return dao.findAll();
    }

    public Optional<Account> findById(UUID id) {
        return dao.findById(id);
    }

    public int create(Account entity) {
        return dao.create(entity);
    }

    public int update(Account entity) {
        return dao.update(entity);
    }

    public int delete(UUID id) {
        return dao.delete(id);
    }

}
