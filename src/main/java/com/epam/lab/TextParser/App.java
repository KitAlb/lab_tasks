package com.epam.lab.TextParser;

import com.epam.lab.TextParser.controller.Controller;

public class App {

    public static void main(String[] args) {
        Controller controller = new Controller();
        controller.initialize();
        controller.showMenu();
    }
}
