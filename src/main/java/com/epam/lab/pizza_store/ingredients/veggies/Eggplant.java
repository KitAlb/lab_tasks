package com.epam.lab.pizza_store.ingredients.veggies;

public class Eggplant implements Veggies {
    @Override
    public String toString() {
        return "Eggplant";
    }
}
