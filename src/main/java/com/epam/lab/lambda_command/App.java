package com.epam.lab.lambda_command;

public class App {
    public static void main(String[] args) {
        Invoker invoker = new Invoker();

        System.out.println(invoker.executeOperation(new Multiply5("text")));
        System.out.println(invoker.executeOperation(new Multiply10("text")));

        // lambda
        System.out.println(invoker.executeOperation(() -> "some text"));

        // method reference
        System.out.println(invoker.executeOperation(App::someMethod));

        //anonymous class
        System.out.println(invoker.executeOperation(new Action() {
            @Override
            public String perform() {
                return "text (Anonymous class)";
            }

        }));
    }

    private static String someMethod() {
        return "text".repeat(3);
    }
}
