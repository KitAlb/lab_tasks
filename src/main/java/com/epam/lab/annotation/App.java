package com.epam.lab.annotation;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class App {
    public static void main(String[] args) {
        MyClass myClass = new MyClass();

        int field = myClass.getField();

        try {
            Field f = myClass.getClass().getDeclaredField("field");
            MyAnnotation a = f.getAnnotation(MyAnnotation.class);

            System.out.println("Value1 = " + a.value1());

            Method methodSum = myClass.getClass().getDeclaredMethod("sum", Integer.class, Integer.class);
            System.out.println("Sum = " + methodSum.invoke(myClass, 5, 6));

            Method methodConcat = myClass.getClass().getDeclaredMethod("concat", String.class, String.class);
            System.out.println("Concat = " + methodConcat.invoke(null, "String1", "String2"));

            Method methodHello = myClass.getClass().getDeclaredMethod("hello");
            System.out.println("Hello = " + methodHello.invoke(myClass));

            f.set(myClass, 45);
            System.out.println("f value = " + f.get(myClass));

            ClassInfo sInfo = new ClassInfo("Test");
            sInfo.printInfo();

            ClassInfo myClassInfo = new ClassInfo(myClass);
            myClassInfo.printInfo();

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
}
