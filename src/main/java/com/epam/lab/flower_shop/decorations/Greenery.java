package com.epam.lab.flower_shop.decorations;

public class Greenery implements Decoration {
    @Override
    public double getPrice() {
        return 3;
    }
}
