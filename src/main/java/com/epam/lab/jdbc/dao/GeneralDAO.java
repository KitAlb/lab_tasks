package com.epam.lab.jdbc.dao;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.List;

public interface GeneralDAO<T, ID> {
    List<T> findAll() throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException;
    T findById(ID id) throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException;
    int create(T entity) throws SQLException;
    int update(T entity) throws SQLException;
    int delete(ID id) throws SQLException;
}
