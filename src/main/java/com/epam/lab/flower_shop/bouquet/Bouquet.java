package com.epam.lab.flower_shop.bouquet;

import com.epam.lab.flower_shop.decorations.Decoration;
import com.epam.lab.flower_shop.flowers.Flower;

import java.util.List;

import static com.epam.lab.flower_shop.bouquet.Constants.GATHERING_PRICE;

public interface Bouquet {
    default double getPrice() {
        return GATHERING_PRICE +
                getFlowers().stream()
                        .map(Flower::getPrice)
                        .reduce((double) 0, Double::sum) +
                getDecorations().stream()
                        .map(Decoration::getPrice)
                        .reduce((double) 0, Double::sum);
    };
    List<Flower> getFlowers();
    List<Decoration> getDecorations();
}
