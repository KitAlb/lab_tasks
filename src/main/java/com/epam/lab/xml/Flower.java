package com.epam.lab.xml;

public class Flower {
    String Name;
    SoilType soil;
    String origin;
    VisualParameters visualParameters;
    GrowingTips growingTips;
    MultiplyingType multiplying;
}
