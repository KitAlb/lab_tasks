package com.epam.lab.mobile_network.dao;

import com.epam.lab.mobile_network.model.Tariff;
import com.epam.lab.mobile_network.model.type.TariffType;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface TariffDAO extends BaseDAO<Tariff, UUID> {
    Optional<List<Tariff>> findByName(String name);
    Optional<List<Tariff>> findByType(TariffType type);
    Optional<Tariff> findByUserId(UUID userId);
}
