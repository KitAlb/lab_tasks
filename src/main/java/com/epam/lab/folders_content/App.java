package com.epam.lab.folders_content;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class App {

    private static Logger logger = LogManager.getLogger("ConsoleLogger");

    public static void main(String[] args) {
        DirectoryExplorer explorer = new DirectoryExplorer(logger);
        explorer.start();
    }

}
