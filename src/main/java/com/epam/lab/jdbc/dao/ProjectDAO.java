package com.epam.lab.jdbc.dao;

import com.epam.lab.jdbc.model.ProjectEntity;

public interface ProjectDAO extends GeneralDAO<ProjectEntity, String> {
}
