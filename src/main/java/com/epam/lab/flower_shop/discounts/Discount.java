package com.epam.lab.flower_shop.discounts;

public interface Discount {

    double getDiscount();

}
