package com.epam.lab.game.kalaha.wire.board.type;

public enum PitType {
    PLAYER,
    KALAH
}
