package com.epam.lab.arrays;

import java.util.*;

class Array {
    private static Random random = new Random();
    private static int BOUND = 20;

    public static int[] createArray() {
        int[] array = new int[random.nextInt(BOUND) + 1];

        for (int i = 0; i < array.length; i++) {
            int number = random.nextInt(100);
            array[i] = number;
        }

        return array;
    }

    private static int countCommonNumbers(int[] firstArray, int[] secondArray) {
        int counter = 0;

        for (int firstNumber : firstArray) {
            for (int secondNumber : secondArray) {
                if (secondNumber == firstNumber) {
                    counter++;
                    break;
                }
            }
        }

        return counter;
    }

    public static int countUniqueNumbers(int[] firstArray, int[] secondArray) {
        int commonCount = countCommonNumbers(firstArray, secondArray);

        return firstArray.length + secondArray.length - (2 * commonCount);
    }

    public static int[] createArrayWithCommonNumbers(int[] firstArray, int[] secondArray) {
        int[] firstArraySorted = getSortedArrayWithoutDuplicates(firstArray);
        int[] secondArraySorted = getSortedArrayWithoutDuplicates(secondArray);

        int[] array = new int[countCommonNumbers(firstArraySorted, secondArraySorted)];

        int i = 0;
        for (int firstNumber : firstArraySorted) {
            for (int secondNumber : secondArraySorted) {
                if (secondNumber == firstNumber) {
                    array[i] = secondNumber;
                    i++;
                }
            }
        }

        return array;
    }

    public static int[] createArrayWithUniqueNumbers(int[] firstArray, int[] secondArray) {
        int[] firstArraySorted = getSortedArrayWithoutDuplicates(firstArray);
        int[] secondArraySorted = getSortedArrayWithoutDuplicates(secondArray);

        int uniqueNumbers = countUniqueNumbers(firstArraySorted, secondArraySorted);

        int[] array = new int[uniqueNumbers];

        int i = 0;
        for (int firstNumber : firstArraySorted) {
            if (isUnique(firstNumber, secondArraySorted)) {
                array[i] = firstNumber;
                i++;
            }
        }

        for (int firstNumber : secondArraySorted) {
            if (isUnique(firstNumber, firstArraySorted)) {
                array[i] = firstNumber;
                i++;
            }
        }

        return array;
    }

    public static int[] deleteDuplicates(int[] array, int occurrenceCounter) {
        int[] newArray = new int[array.length - countDuplicates(array, occurrenceCounter)];

        int i = 0;
        while (i < newArray.length) {
            for (int number : array) {
                int numberCounter = 0;

                for (int value : array) {
                    if (value == number) {
                        numberCounter++;
                    }
                }

                if (numberCounter <= occurrenceCounter) {
                    newArray[i] = number;
                    i++;
                }
            }
        }

        return newArray;
    }

    public static int[] clearDuplicates(int[] array) {
        int[] newArray = new int[array.length - countDuplicatesSuccessively(array)];

        int i = 0;
        int j = 0;
        while (j < array.length) {
            newArray[i] = array[j];
            while (j < array.length - 1 && array[j] == array[j + 1]) {
                j++;
            }
            j++;
            i++;
        }

        return newArray;
    }

    private static boolean isUnique(int value, int[] array) {
        boolean isUnique = true;
        for (int number : array) {
            if (number == value) {
                isUnique = false;
                break;
            }
        }
        return isUnique;
    }

    private static int countDuplicates(int[] array, int occurrenceCounter) {
        int counter = 0;

        for (int number : array) {
            int numberCounter = 0;

            for (int value : array) {
                if (value == number) {
                    numberCounter++;
                }
            }

            if (numberCounter > occurrenceCounter) {
                counter++;
            }
        }

        return counter;
    }

    private static int countDuplicatesSuccessively(int[] array) {
        int counter = 0;

        for (int i = 0; i < array.length; i++) {
            int j = 1;
            int duplicateCounter = 0;
            while (i + j < array.length && array[i] == array[i + j]) {
                duplicateCounter++;
                j++;
            }
            counter+= duplicateCounter;
        }

        return counter;
    }

    private static int[] getSortedArrayWithoutDuplicates(int[] array) {
        int[] arrayCopy = Arrays.copyOf(array, array.length);
        Arrays.sort(arrayCopy);
        return clearDuplicates(arrayCopy);
    }

}
