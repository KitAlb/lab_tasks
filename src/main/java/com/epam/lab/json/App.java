package com.epam.lab.json;

import com.epam.lab.json.orangery.Orangery;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.everit.json.schema.Schema;
import org.everit.json.schema.ValidationException;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;

public class App {

    private static Logger logger = LogManager.getLogger("ConsoleLogger");

    public static void main(String[] args) {
        loadJson();
        validateJsonWithSchema("orangery.json");
        validateJsonWithSchema("orangery_invalid.json");
    }

    private static void loadJson() {
        ObjectMapper objectMapper = new ObjectMapper();

        try {
            logger.info(StringUtils.repeat("-", 30));
            logger.info("Loading Orangery from orangery.json file...");
            URL fileURL = App.class.getClassLoader().getResource("orangery.json");
            String text = Files.readString(Path.of(fileURL.toURI()), Charset.defaultCharset());
            Orangery orangery = objectMapper.readValue(text, Orangery.class);
            logger.info(orangery);
            logger.info(StringUtils.repeat("-", 30));
        } catch (Exception ex) {
            logger.error("Cannot parse file: {}", ex.getMessage());
        }
    }

    private static void validateJsonWithSchema(String fileName) {
        try {
            logger.info(StringUtils.repeat("-", 30));
            logger.info("Validating {} file using orangery_schema.json schema file...", fileName);
            JSONObject jsonSchema = new JSONObject(
                    new JSONTokener(App.class.getResourceAsStream("/orangery_schema.json")));
            JSONObject jsonSubject = new JSONObject(
                    new JSONTokener(App.class.getResourceAsStream("/" + fileName)));

            Schema schema = SchemaLoader.load(jsonSchema);
            schema.validate(jsonSubject);
            logger.info("File is valid");
            logger.info(StringUtils.repeat("-", 30));
        } catch (ValidationException ex) {
            logger.error("Schema validation error: {}", ex.getMessage());
            printCausingExceptions(ex);
            logger.info(StringUtils.repeat("-", 30));
        }
    }

    private static void printCausingExceptions(ValidationException ex) {
        ex.getCausingExceptions()
                .forEach(exception -> {
                    if (CollectionUtils.isNotEmpty(exception.getCausingExceptions())) {
                        printCausingExceptions(exception);
                    } else {
                        logger.error(exception.getMessage());
                    }
                });
    }

}
