package com.epam.lab.annotation;

public class MyClass {
    @MyAnnotation(value1 = 2)
    int field;

    int getField() {
        return this.field;
    }

    Integer sum(Integer a, Integer b) {
        return a + b;
    }

    static String concat(String a, String b) {
        return a + b;
    }

    void hello() {
        System.out.println("Hello");
    }
}
