package com.epam.lab.game.kalaha.wire.player;

import com.epam.lab.game.kalaha.GameState;

import java.util.Scanner;

public class ConsolePlayer implements Player {

    private String name;

    public ConsolePlayer(String name) {
        this.name = name;
    }

    @Override
    public int takeTurn(GameState state) {
        if(state.isExtraTurn()) {
            System.out.println("Extra turn");
        }

        Scanner sc = new Scanner(System.in);
        System.out.println(state);
        System.out.println("Player [" + this.name + "] turn (1-6): ");

        while (!sc.hasNextInt()) {
            sc.next();
        }

        return sc.nextInt();
    }

    @Override
    public String toString() {
        return "ConsolePlayer{" +
                "name='" + name + '\'' +
                '}';
    }

}
