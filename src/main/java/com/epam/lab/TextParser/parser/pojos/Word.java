package com.epam.lab.TextParser.parser.pojos;

import org.apache.commons.lang3.RegExUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

public class Word implements Comparable<Word> {

    private String value;

    public Word(String value) {
        this.value = StringUtils.trim(value);
    }

    public String getValue() {
        return this.value;
    }

    public float getPercentage(String regex) {
        String wordWithoutVowels = RegExUtils.removeAll(getValue(), regex);
        return 1 - ((float) wordWithoutVowels.length() / (float) getValue().length());
    }

    public Boolean isStartsWithAny(String[] strings) {
        return StringUtils.startsWithAny(
                StringUtils.lowerCase(getValue()),
                strings);
    }

    @Override
    public String toString() {
        return this.value;
    }

    @Override
    public int compareTo(Word o) {
        return StringUtils.compare(this.getValue(), o.getValue());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Word word = (Word) o;
        return Objects.equals(value, word.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
