package com.epam.lab.String_Container;

import java.util.Arrays;

public class Container {

    private String[] strings = new String[2];
    private int elementsCount = 0;
    private static int SIZE_STEP = 2;

    Container() {}

    public void addString(String string) {
        if (this.elementsCount == this.strings.length) {
            increaseSize();
        }
        this.strings[elementsCount] = string;
        this.elementsCount++;
    }

    public String getString(int index) {
        return strings[index];
    }

    private void increaseSize() {
        this.strings = Arrays.copyOf(this.strings, this.strings.length + SIZE_STEP);
    }
}
