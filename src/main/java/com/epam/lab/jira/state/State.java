package com.epam.lab.jira.state;

import com.epam.lab.jira.task.Task;

public interface State {

  default void open(Task task) {
    System.out.println("open - is not allowed");
  }

  default void progress(Task task) {
    System.out.println("progress - is not allowed");
  }

  default void block(Task task) {
    System.out.println("block - is not allowed");
  }

  default void resolve(Task task) {
    System.out.println("resolve - is not allowed");
  }

  default void close(Task task) {
    System.out.println("close - is not allowed");
  }

  default void verify(Task task) {
    System.out.println("verify - is not allowed");
  }

  default void review(Task task) {
    System.out.println("review - is not allowed");
  }
}
