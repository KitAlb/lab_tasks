package com.epam.lab.Comparable.boardgame;

import org.apache.commons.lang3.StringUtils;

public class Boardgame implements Comparable<Boardgame> {
    private String name;
    private String publisher;

    public Boardgame(String name, String publisher) {
        this.name = name;
        this.publisher = publisher;
    }

    public String getPublisher() {
        return this.publisher;
    }

    @Override
    public int compareTo(Boardgame game) {
        return StringUtils.compareIgnoreCase(this.name, game.name);
    }

    @Override
    public String toString() {
        return "\nName = " + this.name + "; Publisher = " + this.publisher;
    }
}
