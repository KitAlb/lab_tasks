package com.epam.lab.pizza_store.store.lviv;

import com.epam.lab.pizza_store.ingredients.PizzaIngredientsFactory;
import com.epam.lab.pizza_store.ingredients.cheese.Brynza;
import com.epam.lab.pizza_store.ingredients.cheese.Cheese;
import com.epam.lab.pizza_store.ingredients.cheese.Mozzarella;
import com.epam.lab.pizza_store.ingredients.cheese.ParmigianoReggiano;
import com.epam.lab.pizza_store.ingredients.clams.Clams;
import com.epam.lab.pizza_store.ingredients.clams.PacificRazorClams;
import com.epam.lab.pizza_store.ingredients.dough.Dough;
import com.epam.lab.pizza_store.ingredients.dough.ThinCrustDough;
import com.epam.lab.pizza_store.ingredients.pepperoni.Chorizo;
import com.epam.lab.pizza_store.ingredients.pepperoni.Pepperoni;
import com.epam.lab.pizza_store.ingredients.pepperoni.SalamiPepperoni;
import com.epam.lab.pizza_store.ingredients.sauce.PestoSauce;
import com.epam.lab.pizza_store.ingredients.sauce.Sauce;
import com.epam.lab.pizza_store.ingredients.veggies.*;

import java.util.List;

public class LvivPizzaIngredientsFactory implements PizzaIngredientsFactory {
    @Override
    public Dough createDough() {
        return new ThinCrustDough();
    }

    @Override
    public Sauce createSauce() {
        return new PestoSauce();
    }

    @Override
    public List<Cheese> createCheese() {
        return List.of(
                new Mozzarella(),
                new Brynza(),
                new ParmigianoReggiano()
        );
    }

    @Override
    public List<Veggies> createVeggies() {
        return List.of(
                new Tomato(),
                new SweetPepper(),
                new Olives(),
                new ChiliPepper(),
                new JalapenoPepper()
        );
    }

    @Override
    public List<Pepperoni> createPepperoni() {
        return List.of(new Chorizo(), new SalamiPepperoni());
    }

    @Override
    public List<Clams> createClams() {
        return List.of(new PacificRazorClams());
    }
}
