package com.epam.lab.json.orangery;

public class GrowingTips {
    private byte temperature;
    private Light light;
    private short watering;

    public byte getTemperature() {
        return temperature;
    }

    public void setTemperature(byte temperature) {
        this.temperature = temperature;
    }

    public Light getLight() {
        return light;
    }

    public void setLight(Light light) {
        this.light = light;
    }

    public short getWatering() {
        return watering;
    }

    public void setWatering(short watering) {
        this.watering = watering;
    }

    @Override
    public String toString() {
        return "GrowingTips{" +
                "temperature=" + temperature +
                ", light=" + light +
                ", watering=" + watering +
                '}';
    }
}
