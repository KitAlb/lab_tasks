package com.epam.lab.Logger_Task;

import com.epam.lab.Logger_Task.loggers.*;

import java.util.HashSet;
import java.util.Set;

public class App
{
    public static void main( String[] args ) {

        Set<MyLogger> loggers = new HashSet<>();

        loggers.add(new LoggerDebug());
        loggers.add(new LoggerInfo());
        loggers.add(new LoggerWarn());
        loggers.add(new LoggerSms());

        loggers.forEach(MyLogger::log);

    }
}
