package com.epam.lab.Exceptions_Plants.enums;

import com.epam.lab.Exceptions_Plants.exceptions.ColorException;

import java.util.Arrays;

public enum Color {
    GREEN("green"),
    RED("red"),
    WHITE("white"),
    YELLOW("yellow"),
    BLUE("blue");

    private final String value;

    Color(final String value) {
        this.value = value;
    }

    public static Color of(String string) {
        return Arrays.stream(values())
                .filter(color -> color.toString().equalsIgnoreCase(string))
                .findFirst()
                .orElseThrow(() -> new ColorException(string));
    }

    @Override
    public String toString() {
        return value;
    }
}