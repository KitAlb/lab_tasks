package com.epam.lab.Comparable.comparator;

import com.epam.lab.Comparable.boardgame.Boardgame;
import org.apache.commons.lang3.StringUtils;

import java.util.Comparator;

public class PublisherComparator implements Comparator<Boardgame> {
    @Override
    public int compare(Boardgame o1, Boardgame o2) {
        return StringUtils.compareIgnoreCase(o1.getPublisher(), o2.getPublisher());
    }
}
