package com.epam.lab.Logger_Task.loggers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LoggerSms implements MyLogger {

    private static final Logger logger = LogManager.getLogger(LoggerSms.class);

    @Override
    public void log() {
        logger.fatal("Fatal error");
        logger.info("Info message");
    }
}
