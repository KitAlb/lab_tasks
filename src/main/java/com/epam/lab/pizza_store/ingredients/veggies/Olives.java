package com.epam.lab.pizza_store.ingredients.veggies;

public class Olives implements Veggies {
    @Override
    public String toString() {
        return "Olives";
    }
}
