package com.epam.lab.Logger_Task.sms;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class SMSSender {

    private static final String ACCOUNT_SID = "AC163a9244da799fa6db3ecb56322f22fe";
    private static final String AUTH_TOKEN = "c5dc16baae597422fa72789df1a618b6";
    private static final String TO_NUMBER = "+380992661324";
    private static final String FROM_NUMBER = "+18053228105";

    public static void send(String messageStr) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);

        Message.creator(
                new PhoneNumber(TO_NUMBER), // to
                new PhoneNumber(FROM_NUMBER), // from
                messageStr)
                .create();
    }

}
