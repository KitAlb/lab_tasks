package com.epam.lab.OOP_task_Necklace;

import com.epam.lab.OOP_task_Necklace.generator.JewelsGenerator;
import com.epam.lab.OOP_task_Necklace.jewels.Jewel;
import com.epam.lab.OOP_task_Necklace.necklaces.Necklace;

import java.util.List;

public class Main {

    public static void main(String[] args) {
        Necklace myNecklace1 = new Necklace();

        myNecklace1.addAll(JewelsGenerator.getRandomJewels());
        JewelsGenerator.getRandomJewel().ifPresent(myNecklace1::add);

        Necklace myNecklace2 = new Necklace(JewelsGenerator.getRandomJewels());

        printNecklaceProperties(myNecklace1, 0, 70);
        printNecklaceProperties(myNecklace2, 75, 100);
    }

    private static void printNecklaceProperties(Necklace necklace, int min, int max) {
        System.out.println();
        System.out.println("-----------------------------------------------------");
        System.out.println("Necklace's total weight: " + necklace.getTotalWeight() + " kar");
        System.out.println("Necklace's total price: " + necklace.getTotalPrice() + " USD");
        System.out.println();
        System.out.println("Jewels by price:");

        for (Jewel jewel : necklace.getSortedByPrice()) {
            System.out.println(jewel);
        }
        System.out.println();

        List<Jewel> jewels = necklace.searchByTransparency(min, max);
        if (jewels.size() > 0) {
            System.out.println("Jewels with transparency from " + min + " to " + max + ":");

            for (Jewel jewel : jewels) {
                System.out.println(jewel.getClass().getSimpleName() + ", Transparency = " + jewel.getTransparency() + "%");
            }
        } else {
            System.out.println("Necklace doesn't have jewels with transparency from " + min + " to " + max);
        }

        System.out.println("-----------------------------------------------------");
    }
}