package com.epam.lab.TextParser.parser.pojos;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.epam.lab.TextParser.parser.pojos.Constants.NON_ALPHABET_REGEX;

public class Sentence {

    private List<Word> words;
    private String value;

    public Sentence(String value) {
        this.value = StringUtils.trim(value);
    }

    public List<Word> getWords() {
        if (CollectionUtils.isEmpty(this.words)) {
            this.words = parse(this.value);
        }

        return words;
    }

    public String getValue() {
        return this.value;
    }

    public Boolean isContainDoubles() {
        return getWords().stream()
                .anyMatch(word -> Collections.frequency(getWords(), word) > 1);
    }

    public Boolean isContainWord(Word word) {
        return getWords().contains(word);
    }

    public Boolean isEndsWith(Punctuation punctuation) {
        return StringUtils.endsWith(getValue(), punctuation.toString());
    }

    public Set<String> getWordsBySize(int size) {
        return getWords().stream()
                .filter(word -> word.getValue().length() == size)
                .map(Word::getValue)
                .collect(Collectors.toSet());
    }

    public Optional<String> getLongestWord() {
        return getWords().stream()
                .max(Comparator.comparingInt(word -> word.getValue().length()))
                .map(Word::getValue);
    }

    public Optional<String> getFirstWordStartsWithAny(String[] strings) {
        return getWords().stream()
                .filter(word -> word.isStartsWithAny(strings))
                .findFirst()
                .map(Word::getValue);
    }

    public List<String> getWordsStartsWithAny(String[] strings) {
        return getWords().stream()
                .filter(word -> word.isStartsWithAny(strings))
                .map(Word::getValue)
                .collect(Collectors.toList());
    }

    public long getWordCount(String word) {
        return getWords().stream()
                .filter(w -> StringUtils.equalsIgnoreCase(w.getValue(), word))
                .count();
    }

    private List<Word> parse(String value) {
        return Arrays.stream(value.split(NON_ALPHABET_REGEX))
                .map(Word::new)
                .collect(Collectors.toList());
    }
}
