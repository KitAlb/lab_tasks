package com.epam.lab.flower_shop.bouquet.decorator.implementation;

import com.epam.lab.flower_shop.bouquet.Bouquet;
import com.epam.lab.flower_shop.bouquet.decorator.BouquetDecorator;

public class Discount extends BouquetDecorator {
    public Discount(Bouquet bouquet) {
        super(bouquet);
    }

    public Discount(Bouquet bouquet, double discount) {
        this(bouquet);

        if (discount > bouquet.getPrice()) {
            discount = bouquet.getPrice();
        }

        if (discount < 0) {
            discount = 0;
        }

        setAdditionalPrice(-discount);
    }
}
