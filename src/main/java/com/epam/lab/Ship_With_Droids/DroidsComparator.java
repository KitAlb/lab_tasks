package com.epam.lab.Ship_With_Droids;

import java.util.Comparator;

public class DroidsComparator implements Comparator<Droid> {
    @Override
    public int compare(Droid o1, Droid o2) {
        return Integer.compare(o1.power, o2.power);
    }
}
