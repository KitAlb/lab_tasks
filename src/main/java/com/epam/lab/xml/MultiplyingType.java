package com.epam.lab.xml;

public enum MultiplyingType {
    CUTTINGS,
    LEAVES,
    SEEDS
}
