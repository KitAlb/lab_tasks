package com.epam.lab.lambda_command;

public class Multiply5 implements Action {

    private String targetString;

    Multiply5(String string) {
        this.targetString = string;
    }

    @Override
    public String perform() {
        return targetString.repeat(5);
    }
}
