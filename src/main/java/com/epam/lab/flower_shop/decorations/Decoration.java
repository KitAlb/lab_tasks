package com.epam.lab.flower_shop.decorations;

public interface Decoration {
    double getPrice();
}
