package com.epam.lab.jdbc.transformer;

import com.epam.lab.jdbc.model.annotation.Column;
import com.epam.lab.jdbc.model.annotation.PrimaryKeyComposite;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.*;

public class Transformer<T> {
    private final Class<T> clazz;

    public Transformer(Class<T> clazz) {
        this.clazz = clazz;
    }

    public T fromResultSetToEntity(ResultSet rs)
            throws SQLException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        T dto = (T) clazz.getConstructor().newInstance();
        for (Field field : clazz.getDeclaredFields()) {
            Column col = field.getAnnotation(Column.class);
            if (col != null) {
                field.setAccessible(true);
                String dbValue = rs.getString(col.name());
                if (dbValue == null) {
                    continue;
                }
                field.set(dto, getValueObject(field, dbValue));
            }

            PrimaryKeyComposite pk = field.getAnnotation(PrimaryKeyComposite.class);
            if (pk != null) {
                field.setAccessible(true);
                field.set(dto, getPrimaryKeyObject(field, rs));
            }
        }

        return dto;
    }

    private Object getPrimaryKeyObject(Field field, ResultSet rs) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException, SQLException {
        Object dto = field.getType().getConstructor().newInstance();
        for (Field subField : field.getType().getDeclaredFields()) {
            Column col = subField.getAnnotation(Column.class);
            if (col != null) {
                subField.setAccessible(true);
                String dbValue = rs.getString(col.name());
                if (dbValue == null) {
                    continue;
                }
                subField.set(dto, subField.getType().getConstructor(String.class).newInstance(dbValue));
            }
        }

        return dto;
    }

    private Object getValueObject(Field field, String strValue) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        try {
            Constructor constructor = field.getType().getConstructor(String.class);
            return  constructor.newInstance(strValue);
        } catch (NoSuchMethodException ignored) {
        }

        try {
            Method valueOfMethod = field.getType().getDeclaredMethod("valueOf", String.class);

            return valueOfMethod.invoke(field, strValue);
        } catch (NoSuchMethodException ignored) {
        }

        return null;
    }
}
