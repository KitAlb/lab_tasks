package com.epam.lab.pizza_store.ingredients.veggies;

public class Onion implements Veggies {
    @Override
    public String toString() {
        return "Onion";
    }
}
