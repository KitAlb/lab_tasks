package com.epam.lab.game;

import com.epam.lab.game.kalaha.Game;
import com.epam.lab.game.kalaha.wire.player.ConsolePlayer;

import static com.epam.lab.game.kalaha.Constants.SEEDS;

public class Main {

    public static void main(String[] args) throws Exception {

        printRules();

        Game game = new Game(new ConsolePlayer("PlayerA"), new ConsolePlayer("PlayerB"), SEEDS);
        game.start();
        System.out.println("The winner is: " + game.getWinner());

    }

    private static void printRules() {
        System.out.println("                              ---=== KALAHA ===---");
        System.out.println("1. At the beginning of the game, x seeds are placed in each house.");
        System.out.println("2. Each player controls the six houses and their seeds on the player's side of the board.\n" +
                "The player's score is the number of seeds in the store to their right.");
        System.out.println("3. Players take turns sowing their seeds. On a turn, the player removes all seeds from one\n" +
                "of the houses under their control. Moving counter-clockwise, the player drops one seed in each house\n" +
                "in turn, including the player's own store but not their opponent's.");
        System.out.println("4. When one player no longer has any seeds in any of their houses, the game ends.\n" +
                "The other player moves all remaining seeds to their store, and the player with the most seeds\n" +
                "in their store wins.");
        System.out.println("5. If the last sown seed lands in the player's store, the player gets an additional move.\n" +
                "There is no limit on the number of moves a player can make in their turn.");
        System.out.println("------------------------------------------------------------------------------------------");
        System.out.println();
    }

}
