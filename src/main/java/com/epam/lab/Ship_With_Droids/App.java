package com.epam.lab.Ship_With_Droids;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class App {
    private static Logger logger = LogManager.getLogger("ConsoleLogger");

    public static void main(String[] args) {
        Ship myShip = new Ship();
        ArrayList<Droid> droids = new ArrayList<>();
        BB8 myBB8 = new BB8();
        C3PO myC3PO = new C3PO();
        R2D2 myR2D2 = new R2D2();

        droids.add(myR2D2);
        droids.add(myC3PO);
        droids.add(myBB8);

        myShip.putDroids(droids);

        logger.info(Droid.getNameAndPower(myShip.getDroid(0)));
        logger.info(Droid.getNameAndPower(myShip.getDroid(1)));
        logger.info(Droid.getNameAndPower(myShip.getDroid(2)));

        logger.info("Priority Queue Example");
        PriorityQueue<? extends Droid> queue = new PriorityQueue<>(new DroidsComparator());
        queue.add(myR2D2);
        queue.add(myBB8);
        queue.add(myC3PO);

        logger.info("Peek:");
        logger.info(Droid.getNameAndPower(queue.peek()));
        logger.info(Droid.getNameAndPower(queue.peek()));
        logger.info(Droid.getNameAndPower(queue.peek()));

        logger.info("Poll:");
        logger.info(Droid.getNameAndPower(queue.poll()));
        logger.info(Droid.getNameAndPower(queue.poll()));
        logger.info(Droid.getNameAndPower(queue.poll()));

        logger.info("Write droids to file");
        serializeDroids(droids);

        logger.info("Read droids from file");
        deserializeDroids().ifPresent(droidsList -> logger.info(droidsList));
    }

    private static void serializeDroids(ArrayList<Droid> droids) {
        try (FileOutputStream fileOutputStream = new FileOutputStream("droids.ser");
             BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fileOutputStream);
             ObjectOutputStream objectOutputStream = new ObjectOutputStream(bufferedOutputStream)
        ) {
            objectOutputStream.writeObject(droids);
        } catch (IOException ex) {
            logger.error("Cannot serialize droids to file: {}", ex.getMessage());
        }
    }

    private static Optional<List<Droid>> deserializeDroids() {
        try (FileInputStream fileInputStream = new FileInputStream("droids.ser");
             BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
             ObjectInputStream objectInputStream = new ObjectInputStream(bufferedInputStream)
        ) {
            return Optional.ofNullable((List<Droid>)objectInputStream.readObject());
        } catch (Exception ex) {
            logger.error("Cannot deserialize droids from file: {}", ex.getMessage());
        }

        return Optional.empty();
    }
}
