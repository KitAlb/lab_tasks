package com.epam.lab.pizza_store.pizza;

import com.epam.lab.pizza_store.ingredients.PizzaIngredientsFactory;
import org.apache.logging.log4j.Logger;

public class VeggiesPizza extends Pizza {
    private PizzaIngredientsFactory ingredientsFactory;

    public VeggiesPizza(Logger logger, String name, PizzaIngredientsFactory ingredientsFactory) {
        super(logger, name);
        this.ingredientsFactory = ingredientsFactory;
    }

    @Override
    public void prepare() {
        dough = ingredientsFactory.createDough();
        sauce = ingredientsFactory.createSauce();
        cheeses.addAll(ingredientsFactory.createCheese());
        veggies.addAll(ingredientsFactory.createVeggies());
        logger.info("Preparing {}", this);
    }
}
