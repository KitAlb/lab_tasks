package com.epam.lab.OOP_task_Necklace.jewels;

public class Opal extends Jewel {

    private static final int PRICE_PER_KAR = 8;
    private static final int TRANSPARENCY = 90;

    public Opal(int weight) {
        this(PRICE_PER_KAR, weight, TRANSPARENCY);
    }

    private Opal(int pricePerKar, int weight, int transparency) {
        super(pricePerKar, weight, transparency);
    }

}
