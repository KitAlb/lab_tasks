package com.epam.lab.flower_shop.bouquet.decorator.implementation;

import com.epam.lab.flower_shop.bouquet.Bouquet;
import com.epam.lab.flower_shop.bouquet.decorator.BouquetDecorator;
import com.epam.lab.flower_shop.decorations.Box;

public class BoxPackaging extends BouquetDecorator {
    public BoxPackaging(Bouquet bouquet) {
        super(bouquet);
        addDecoration(new Box());
    }
}
