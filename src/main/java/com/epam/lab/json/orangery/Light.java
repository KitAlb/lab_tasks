package com.epam.lab.json.orangery;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum Light {
    @JsonProperty("yes")
    YES,
    @JsonProperty("no")
    NO
}
