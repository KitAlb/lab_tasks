package com.epam.lab.jira.state;

import com.epam.lab.jira.task.Task;

public class InReview implements State {

    public void progress(Task task) {
        task.setState(new InProgress());
        System.out.println("Reject review");
    }

    public void resolve(Task task) {
        task.setState(new Resolved());
        System.out.println("Review complete");
    }

}
