package com.epam.lab.string_tasks.check_sentences;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class App {
    private static final String REGEX = "^[A-Z].*[\\.]$";
    private static final String INPUT1 = "Check this sentence.";
    private static final String INPUT2 = "check this sentence.";
    private static final String INPUT3 = "Check this sentence";


    public static void main( String args[] ) {
        System.out.println("Current REGEX is: "+REGEX);
        System.out.println("Current INPUT is: "+INPUT1);
        System.out.println("matches(): "+Pattern.matches(REGEX,INPUT1));
        System.out.println("Current INPUT is: "+INPUT2);
        System.out.println("matches(): "+Pattern.matches(REGEX,INPUT2));
        System.out.println("Current INPUT is: "+INPUT3);
        System.out.println("matches(): "+Pattern.matches(REGEX,INPUT3));
    }
}
