package com.epam.lab.TextParser.parser.pojos;

public enum Punctuation {
    BANG("!"),
    BANG_EQ("!="),
    BANG_EQ_EQ("!=="),
    PCT("%"),
    PCT_EQ("%="),
    AMP("&"),
    AMP_AMP("&&"),
    AMP_AMP_EQ("&&="),
    AMP_EQ("&="),
    LPAREN("("),
    RPAREN(")"),
    AST("*"),
    AST_EQ("*="),
    PLUS("+"),
    PLUS_PLUS("++"),
    PLUS_EQ("+="),
    COMMA(","),
    MINUS("-"),
    MINUS_MINUS("--"),
    MINUS_EQ("-="),
    DOT("."),
    DOT_DOT(".."),
    ELIPSIS("..."),
    COLON(":"),
    COLON_COLON("::"),
    SEMI(";"),
    LT("<"),
    LT_LT("<<"),
    LT_LT_EQ("<<="),
    LT_EQ("<="),
    EQ("="),
    EQ_EQ("=="),
    EQ_EQ_EQ("==="),
    GT(">"),
    GT_EQ(">="),
    GT_GT(">>"),
    GT_GT_EQ(">>="),
    GT_GT_GT(">>>"),
    GT_GT_GT_EQ(">>>="),
    QMARK("?"),
    LSQUARE("["),
    RSQUARE("]"),
    CARET("^"),
    CARET_EQ("^="),
    LCURLY("{"),
    PIPE("|"),
    PIPE_EQ("|="),
    PIPE_PIPE("||"),
    PIPE_PIPE_EQ("||="),
    RCURLY("}"),
    TILDE("~"),
    SLASH("/"),
    SLASH_EQ("/=");

    private String value;

    Punctuation(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return this.value;
    }
}
