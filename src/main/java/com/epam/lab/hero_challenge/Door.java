package com.epam.lab.hero_challenge;

public class Door {
    private DoorType type;
    private int strength;
    private boolean isVisited = false;
    private int number;

    Door(int number, DoorType type, int strength){
        this.number = number;
        this.type = type;
        this.strength = strength;
    }

    public int getNumber() {
        return number;
    }

    public DoorType getType() {
        return type;
    }

    public int getStrength() {
        return strength;
    }

    public boolean isVisited() {
        return isVisited;
    }

    public int visit() {
        this.isVisited = true;
        return this.strength;
    }
}
