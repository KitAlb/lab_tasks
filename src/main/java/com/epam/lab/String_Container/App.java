package com.epam.lab.String_Container;

import org.apache.commons.lang3.time.StopWatch;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class App {

    private static Logger logger = LogManager.getLogger("ConsoleLogger");

    public static void main(String[] args) {
        StopWatch stopWatch = new StopWatch();

        Container container = new Container();
        stopWatch.start();
        container.addString("I am a new array");
        container.addString("Hello");
        container.addString("How are you?");
        logger.info(container.getString(0));
        logger.info(container.getString(1));
        logger.info(container.getString(2));
        stopWatch.stop();
        logger.debug("Container work time: " + stopWatch.getTime(TimeUnit.MICROSECONDS) + "mks");

        ArrayList<String> strings = new ArrayList<>();
        stopWatch.reset();
        stopWatch.start();
        strings.add("I am a new array");
        strings.add("Hello");
        strings.add("How are you?");
        logger.info(strings.get(0));
        logger.info(strings.get(1));
        logger.info(strings.get(2));
        stopWatch.stop();
        logger.debug("ArrayList work time: " + stopWatch.getTime(TimeUnit.MICROSECONDS) + "mks");
    }
}
