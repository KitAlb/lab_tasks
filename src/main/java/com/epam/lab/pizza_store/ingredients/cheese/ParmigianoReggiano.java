package com.epam.lab.pizza_store.ingredients.cheese;

public class ParmigianoReggiano implements Cheese {
    @Override
    public String toString() {
        return "Parmigiano Reggiano";
    }
}
