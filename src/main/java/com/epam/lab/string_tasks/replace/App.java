package com.epam.lab.string_tasks.replace;

public class App {
    public static void main(String[] args) {
        String text = "yytew jjerwf dfhgwer iiuqeiu jdjhjfj jidfjierwoef";

        String result = text.replaceAll("[aeiou]", "_");

        System.out.println(result);
    }
}
