package com.epam.lab.TextParser.controller;

import com.epam.lab.TextParser.parser.TextParser;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

import static com.epam.lab.TextParser.parser.pojos.Constants.VOWELS;
import static org.apache.commons.lang3.StringUtils.SPACE;

public class Controller {
    private static Logger logger = LogManager.getLogger("ConsoleLogger");
    private static String TAB = "\t";
    private static int MAX_NUMBER_LENGTH = 8;
    private static int DEFAULT_MAX_WORD_LENGTH = 5;

    private Map<String, String> menu;
    private Map<String, ControllerMethod> methodsMenu;
    private static Scanner input = new Scanner(System.in);
    private TextParser parser;

    public Controller() {
        menu = new LinkedHashMap<>();
        menu.put("1", "    1 - Show sentences with double words");
        menu.put("2", "    2 - Print sentences in words count ascending order");
        menu.put("3", "    3 - Find unique word in first sentence");
        menu.put("4", "    4 - Show all words with specified length in questions");
        menu.put("5", "    5 - Replace first word starting with vowel with longest word");
        menu.put("6", "    6 - Print words alphabetically");
        menu.put("7", "    7 - Sort words by vowels percentage");
        menu.put("8", "    8 - Sort words starting from vowels by second letter");
        menu.put("9", "    9 - Sort words by specified letter percentage");
        menu.put("10", "    10 - Count specified words in each sentence");
        menu.put("11", "    11 - Delete max substring that starts and ends with the given symbols");
        menu.put("12", "    12 - Delete all words of given length that start with a consonant");
        menu.put("13", "    13 - Sort the words by the number of occurrences of a given character");
        menu.put("14", "    14 - Find max substring that is a palindrome");
        menu.put("15", "    15 - Delete all occurrences of a start (end) letter in all words");
        menu.put("16", "    16 - Replace all words of a given length with the substring");
        menu.put("Q", "    Q - Exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::showSentenceWithDoubleWords);
        methodsMenu.put("2", this::printSentenceInWordsCountAscendingOrder);
        methodsMenu.put("3", this::findUniqueWordInFirstSentence);
        methodsMenu.put("4", this::showAllWordsInSpecifiedLength);
        methodsMenu.put("5", this::showSentencesWithReplacedWords);
        methodsMenu.put("6", this::printWordsAlphabetically);
        methodsMenu.put("7", this::sortWordsByVowelsPercentage);
        methodsMenu.put("8", this::sortWordsStartedFromVowels);
        methodsMenu.put("9", this::sortWordsByCharPercentage);
        methodsMenu.put("10", this::countWordsOccurrenceBySentence);
        methodsMenu.put("11", this::deleteSubsequence);
        methodsMenu.put("12", this::deleteWordsByLength);
        methodsMenu.put("13", this::sortWordsByCharCountDescending);
        methodsMenu.put("14", this::findMaxPalindrome);
        methodsMenu.put("15", this::deleteEachFirstChar);
        methodsMenu.put("16", this::replaceWordsWithSentence);
        methodsMenu.put("Q", this::exit);
    }

    public void initialize() {
        try {
            URL fileURL = Controller.class.getClassLoader().getResource("TestText.txt");
            String text = Files.readString(Path.of(fileURL.toURI()), Charset.defaultCharset());
            parser = new TextParser(text);
        } catch (Exception ex) {
            logger.error("Exception occurred when launch a program: " + ex.getMessage());
        }
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String key : menu.keySet()) {
            System.out.println(menu.get(key));
        }
    }

    public void showMenu() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point");
            keyMenu = input.nextLine().toUpperCase();

            try {
                methodsMenu.get(keyMenu).run();
            } catch (Exception e) {
                logger.error("Exception: {}", e.getMessage());
            }
        } while (!keyMenu.equals("Q"));
    }

    // ---------- MENU METHODS -------------
    private void showSentenceWithDoubleWords() {
        Optional.ofNullable(parser).ifPresentOrElse(p -> {
            logger.info("Sentences with double words count: " + p.countSentencesWithDoubleWords());
        }, this::printParserNotInitialized);
    }

    private void printSentenceInWordsCountAscendingOrder() {
        Optional.ofNullable(parser).ifPresentOrElse(p -> {
            logger.info("Sentences in ascending order:");
            printCollection(p.getSentencesInAscendingOrder());
        }, this::printParserNotInitialized);
    }

    private void findUniqueWordInFirstSentence() {
        Optional.ofNullable(parser).ifPresentOrElse(p -> {
            p.findFirstSentenceUniqueWord()
                    .ifPresentOrElse(
                            word -> logger.info("Unique word in first sentence: " + word),
                            () -> logger.warn("There is no unique words in first sentence")
                    );
        }, this::printParserNotInitialized);
    }

    private void showAllWordsInSpecifiedLength() {
        Optional.ofNullable(parser).ifPresentOrElse(p -> {
            logger.info("Specify word length");
            int wordLength = input.nextInt();
            logger.info("All words with length {} in questions:", wordLength);
            printCollection(p.getWordsBySizeInQuestions(wordLength));
        }, this::printParserNotInitialized);
    }

    private void showSentencesWithReplacedWords() {
        Optional.ofNullable(parser).ifPresentOrElse(p -> {
            logger.info("Sentences with replaced words:");
            printCollection(p.getSentencesWithReplacesWords());
        }, this::printParserNotInitialized);
    }

    private void printWordsAlphabetically() {
        Optional.ofNullable(parser).ifPresentOrElse(p -> {
            logger.info("All words alphabetically:");
            printStringsTabbed(p.getWordsAlphabetically());
        }, this::printParserNotInitialized);
    }

    private void sortWordsByVowelsPercentage() {
        Optional.ofNullable(parser).ifPresentOrElse(p -> {
            logger.info("Words sorted by vowels percentage:");
            printCollection(p.getWordsSortedByPercentage("[" + VOWELS + "]"));
        }, this::printParserNotInitialized);
    }

    private void sortWordsStartedFromVowels() {
        Optional.ofNullable(parser).ifPresentOrElse(p -> {
            logger.info("Words started from vowels and sorted by second letter:");
            printCollection(p.getWordsSortedBySecondLetter(VOWELS.split("")));
        }, this::printParserNotInitialized);
    }

    private void sortWordsByCharPercentage() {
        Optional.ofNullable(parser).ifPresentOrElse(p -> {
            logger.info("Please input a character:");
            String c = String.valueOf(input.next().charAt(0));
            logger.info("Sort words by character percentage ('{}'):", c);
            printCollection(p.getWordsSortedByLetterPercentage(c));
        }, this::printParserNotInitialized);
    }

    private void countWordsOccurrenceBySentence() {
        Optional.ofNullable(parser).ifPresentOrElse(p -> {
            logger.info("Specify words divided by whitespace");
            String[] words = StringUtils.split(input.nextLine(), SPACE);
            Integer longestWordLength = getLongestWordLength(words);
            p.getWordsCountBySentence(words).entrySet().stream()
                    .sorted(Map.Entry.comparingByValue(new Comparator<Map<Integer, Long>>() {
                        @Override
                        public int compare(Map<Integer, Long> o1, Map<Integer, Long> o2) {
                            return Long.compare(
                                    o2.values().stream().reduce(0L, Long::sum),
                                    o1.values().stream().reduce(0L, Long::sum)
                            );
                        }
                    }))
                    .forEach(entry -> printWordCounts(
                            entry.getKey(),
                            entry.getValue(),
                            longestWordLength > DEFAULT_MAX_WORD_LENGTH ?
                                    longestWordLength :
                                    DEFAULT_MAX_WORD_LENGTH
                    ));

        }, this::printParserNotInitialized);
    }

    private void deleteSubsequence() {
        Optional.ofNullable(parser).ifPresentOrElse(p -> {
            logger.info("Not implemented yet");
        }, this::printParserNotInitialized);
    }

    private void deleteWordsByLength() {
        Optional.ofNullable(parser).ifPresentOrElse(p -> {
            logger.info("Not implemented yet");
        }, this::printParserNotInitialized);
    }

    private void sortWordsByCharCountDescending() {
        Optional.ofNullable(parser).ifPresentOrElse(p -> {
            logger.info("Not implemented yet");
        }, this::printParserNotInitialized);
    }

    private void findMaxPalindrome() {
        Optional.ofNullable(parser).ifPresentOrElse(p -> {
            logger.info("Not implemented yet");
        }, this::printParserNotInitialized);
    }

    private void deleteEachFirstChar() {
        Optional.ofNullable(parser).ifPresentOrElse(p -> {
            logger.info("Not implemented yet");
        }, this::printParserNotInitialized);
    }

    private void replaceWordsWithSentence() {
        Optional.ofNullable(parser).ifPresentOrElse(p -> {
            logger.info("Not implemented yet");
        }, this::printParserNotInitialized);
    }

    private void exit() {
        logger.info("Exiting...");
    }

    // ---------- ADDITIONAL METHODS -------------
    private void printParserNotInitialized() {
        logger.error("Parser is not initialized.");
    }

    private void printWordCounts(String word, Map<Integer, Long> counts, Integer maxWordLength) {
        logger.info("Word | Sentence | Count");
        counts.forEach((key, value) -> logger.info("{} | {} | {}",
                StringUtils.rightPad(word, maxWordLength, SPACE),
                StringUtils.rightPad(String.valueOf(key), MAX_NUMBER_LENGTH, SPACE),
                value
        ));
    }

    private void printCollection(Collection<String> collection) {
        collection.forEach(string -> logger.info(string));
    }

    private void printStringsTabbed(List<String> words) {
        String letter = StringUtils.left(words.get(0), 1);
        for (String word : words) {
            if (!StringUtils.equalsIgnoreCase(StringUtils.left(word, 1), letter)) {
                letter = StringUtils.left(word, 1);
                System.out.print(TAB);
            } else {
                System.out.print(SPACE);
            }
            System.out.print(word);
        }
    }

    private Integer getLongestWordLength(String[] words) {
        return Arrays.stream(words)
                .map(String::length)
                .mapToInt(i -> i)
                .max()
                .orElse(DEFAULT_MAX_WORD_LENGTH);
    }

}
