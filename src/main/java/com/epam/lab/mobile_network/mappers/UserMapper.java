package com.epam.lab.mobile_network.mappers;

import com.epam.lab.mobile_network.model.User;

import java.util.UUID;

public class UserMapper {

    public static User updateInfo(User user, String firstName, String lastName) {
        return User.builder()
                .id(user.getId())
                .firstName(firstName)
                .lastName(lastName)
                .tariffId(user.getTariffId())
                .phoneNumber(user.getPhoneNumber())
                .accountId(user.getAccountId())
                .build();
    }

    public static User updatePhoneNumber(User user, String phoneNumber) {
        return User.builder()
                .id(user.getId())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .tariffId(user.getTariffId())
                .phoneNumber(phoneNumber)
                .accountId(user.getAccountId())
                .build();
    }

    public static User updateTariff(User user, UUID tariffId) {
        return User.builder()
                .id(user.getId())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .tariffId(tariffId)
                .phoneNumber(user.getPhoneNumber())
                .accountId(user.getAccountId())
                .build();
    }

}
