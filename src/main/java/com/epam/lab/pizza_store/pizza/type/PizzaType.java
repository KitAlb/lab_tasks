package com.epam.lab.pizza_store.pizza.type;

public enum PizzaType {

    CHEESE,
    VEGGIE,
    CLAM,
    PEPPERONI

}
