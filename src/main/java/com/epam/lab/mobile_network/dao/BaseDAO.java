package com.epam.lab.mobile_network.dao;

import java.util.List;
import java.util.Optional;

public interface BaseDAO<T, ID> {
    Optional<List<T>> findAll();
    Optional<T> findById(ID id);
    int create(T entity);
    int update(T entity);
    int delete(ID id);
}
