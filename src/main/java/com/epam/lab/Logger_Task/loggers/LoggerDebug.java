package com.epam.lab.Logger_Task.loggers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LoggerDebug implements MyLogger {

    private static final Logger logger = LogManager.getLogger(LoggerDebug.class);

    @Override
    public void log() {
        logger.trace("LoggerDebug: trace");
        logger.debug("LoggerDebug: debug");
        logger.info("LoggerDebug: info");
        logger.warn("LoggerDebug: warn");
        logger.error("LoggerDebug: error");
    }
}
