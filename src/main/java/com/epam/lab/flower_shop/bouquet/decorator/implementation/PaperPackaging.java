package com.epam.lab.flower_shop.bouquet.decorator.implementation;

import com.epam.lab.flower_shop.bouquet.Bouquet;
import com.epam.lab.flower_shop.bouquet.decorator.BouquetDecorator;
import com.epam.lab.flower_shop.decorations.PaperWrapping;

public class PaperPackaging extends BouquetDecorator {
    public PaperPackaging(Bouquet bouquet) {
        super(bouquet);
        addDecoration(new PaperWrapping());
    }
}
