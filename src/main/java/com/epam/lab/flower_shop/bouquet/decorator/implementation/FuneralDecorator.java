package com.epam.lab.flower_shop.bouquet.decorator.implementation;

import com.epam.lab.flower_shop.bouquet.Bouquet;
import com.epam.lab.flower_shop.bouquet.decorator.BouquetDecorator;
import com.epam.lab.flower_shop.decorations.FuneralRibbon;

public class FuneralDecorator extends BouquetDecorator {
    public FuneralDecorator(Bouquet bouquet) {
        super(bouquet);
        addDecoration(new FuneralRibbon());
    }
}
