package com.epam.lab.lambda_command;

public interface Action {
    String perform();
}
