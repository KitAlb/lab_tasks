package com.epam.lab.pizza_store.store.kyiv;

import com.epam.lab.pizza_store.ingredients.PizzaIngredientsFactory;
import com.epam.lab.pizza_store.pizza.*;
import com.epam.lab.pizza_store.pizza.type.PizzaType;
import com.epam.lab.pizza_store.store.PizzaStore;
import org.apache.logging.log4j.Logger;

import java.util.Optional;

public class KyivPizzaStore extends PizzaStore {
    public KyivPizzaStore(Logger logger) {
        super(logger);
    }

    @Override
    protected Optional<Pizza> createPizza(PizzaType type) {
        PizzaIngredientsFactory ingredientsFactory = new KyivPizzaIngredientsFactory();

        switch (type) {
            case CHEESE:
                return Optional.of(new CheesePizza(
                        logger,
                        "Kyiv Style Cheese",
                        ingredientsFactory
                ));
            case CLAM:
                return Optional.of(new ClamPizza(
                        logger,
                        "Kyiv Style Clam",
                        ingredientsFactory
                ));
            case VEGGIE:
                return Optional.of(new VeggiesPizza(
                        logger,
                        "Kyiv Style Veggies",
                        ingredientsFactory));
            case PEPPERONI:
                return Optional.of(new PepperoniPizza(
                        logger,
                        "Kyiv Style Pepperoni",
                        ingredientsFactory));
            default:
                return Optional.empty();
        }
    }
}
