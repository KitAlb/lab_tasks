package com.epam.lab.lambda_command;

public class Multiply10 implements Action {

    private String targetString;

    Multiply10(String string) {
        this.targetString = string;
    }

    @Override
    public String perform() {
        return targetString.repeat(10);
    }
}
