package com.epam.lab.flower_shop.bouquet.decorator.implementation;

import com.epam.lab.flower_shop.bouquet.Bouquet;
import com.epam.lab.flower_shop.bouquet.decorator.BouquetDecorator;

public class BonusCard extends BouquetDecorator {
    public BonusCard(Bouquet bouquet) {
        super(bouquet);
        setAdditionalPrice(-(bouquet.getPrice()*0.1));
    }
}
