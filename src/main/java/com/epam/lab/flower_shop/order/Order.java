package com.epam.lab.flower_shop.order;

import com.epam.lab.flower_shop.bouquet.Bouquet;
import com.epam.lab.flower_shop.order.state.ReceivedState;
import com.epam.lab.flower_shop.order.state.State;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class Order {
    private Logger logger;
    private List<Bouquet> items;
    private State state;

    public Order(Logger logger) {
        this.logger = logger;
        this.state = new ReceivedState();
    }

    public void setState(State state) {
        this.state = state;
    }

    public void pay() {
        logger.info(state.pay(this));
    }

    public void prepare(List<Bouquet> items) {
        this.items = items;
        logger.info(state.prepare(this));
    }

    public void deliver() {
        logger.info(state.deliver(this));
    }

    public void cancel() {
        logger.info(state.cancel(this));
    }

    public void close() {
        logger.info(state.close(this));
    }

}
