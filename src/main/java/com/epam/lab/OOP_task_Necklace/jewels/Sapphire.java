package com.epam.lab.OOP_task_Necklace.jewels;

public class Sapphire extends Jewel {

    private static final int PRICE_PER_KAR = 15;
    private static final int TRANSPARENCY = 80;

    public Sapphire(int weight) {
        this(PRICE_PER_KAR, weight, TRANSPARENCY);
    }

    private Sapphire(int pricePerKar, int weight, int transparency) {
        super(pricePerKar, weight, transparency);
    }

}
