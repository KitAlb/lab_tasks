package com.epam.lab.Add_String_to_Integer_List;

import java.util.ArrayList;
import java.util.List;

public class App {
    public static void main(String[] args) {
        List list = new ArrayList();
        list.add(848);
        list.add("uherhuf");
        List<Integer> integerList = new ArrayList<>();
        integerList.addAll(list);
        System.out.println(integerList);
        System.out.println(integerList.get(0)/2);
        // Will crash the app
        // System.out.println(integerList.get(1)/2);
    }
}

