package com.epam.lab.hero_challenge;

public class Hero {
    private int strength;

    Hero(){
        this.strength = 25;
    }

    public int getStrength() {
        return strength;
    }

    public void increaseStrength(int strength) {
        this.strength += strength;
    }
}
