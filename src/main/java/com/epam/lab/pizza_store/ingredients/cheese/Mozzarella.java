package com.epam.lab.pizza_store.ingredients.cheese;

public class Mozzarella implements Cheese {
    @Override
    public String toString() {
        return "Mozzarella";
    }
}
