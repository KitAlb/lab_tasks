package com.epam.lab.jira.task;

import com.epam.lab.jira.state.Open;
import com.epam.lab.jira.state.State;

public class Task {

    private State state;

    public Task() {
        state = new Open();
    }

    public void setState(State state) {
        this.state = state;
    }

    public void open() {
        state.open(this);
    }

    public void progress() {
        state.progress(this);
    }

    public void block() {
      state.block(this);
    }

    public void resolve() {
      state.resolve(this);
    }

    public void close() {
      state.close(this);
    }

    public void verify() {
      state.verify(this);
    }

    public void review() {
      state.review(this);
    }

}
