package com.epam.lab.folders_content;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Logger;

import javax.print.DocFlavor;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.StringJoiner;
import java.util.stream.Stream;

public class DirectoryExplorer {

    private static int NAME_SIZE = 30;
    private static int TYPE_SIZE = 11;
    private static int SIZE_SIZE = 15;
    private static int TIME_SIZE = 30;
    private static String COMMAND_SEPARATOR = StringUtils.SPACE;
    private static String COLUMN_DELIMITER = "|";
    private static String CD_UP = "..";

    private Logger logger;
    private Path currentPath = Paths.get("").toAbsolutePath();

    public DirectoryExplorer(Logger logger) {
        this.logger = logger;
    }

    public void start() {
        printCurrentPath();
        try (Scanner scanner = new Scanner(System.in)) {
            logger.info("Enter command: {}", Arrays.toString(Command.values()));
            while (processCommand(scanner.nextLine())) {
                printCurrentPath();
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
    }

    private boolean processCommand(String commandString) {
        try {
            String[] commands = StringUtils.split(StringUtils.trim(commandString), COMMAND_SEPARATOR);
            Command command = Command.of(commands[0]);

            switch (command) {
                case CD:
                    if (commands.length <= 1) {
                        logger.error("Directory name or {} expected after cd command.", CD_UP);
                        return true;
                    }
                    return processCd(commands[1]);
                case DIR:
                    return processDir();
                case EXIT:
                    return false;
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }

        return true;
    }

    private boolean processDir() throws IOException {
        printHeader();
        Stream.concat(
                Files.list(currentPath)
                        .filter(path -> Files.isDirectory(path)),
                Files.list(currentPath)
                        .filter(path -> !Files.isDirectory(path)))
                .forEach(path -> {
                            try {
                                printRow(path);
                            } catch (IOException ex) {
                                logger.error(ex.getMessage());
                            }
                        }
                );
        return true;
    }

    private void printHeader() {
        logger.info(getRowString(
                List.of("Name", "Type", "Size (Bytes)", "Last modified time"),
                List.of(NAME_SIZE, TYPE_SIZE, SIZE_SIZE, TIME_SIZE)
        ));
    }

    private void printRow(Path path) throws IOException {
        logger.info(getRowString(
                List.of(
                        path.getFileName().toString(),
                        Files.isDirectory(path) ? FileType.DIRECTORY.toString() : FileType.FILE.toString(),
                        String.valueOf(Files.size(path)),
                        Files.getLastModifiedTime(path).toString()
                ),
                List.of(NAME_SIZE, TYPE_SIZE, SIZE_SIZE, TIME_SIZE)
        ));
    }

    private String getRowString(List<String> items, List<Integer> sizes) {
        StringJoiner joiner = new StringJoiner(COLUMN_DELIMITER, COLUMN_DELIMITER, COLUMN_DELIMITER);
        for (int i = 0; i < items.size(); i++) {
            joiner.add(StringUtils.rightPad(StringUtils.SPACE + items.get(i), sizes.get(i), StringUtils.SPACE));
        }
        return joiner.toString();
    }

    private boolean processCd(String argument) {
        if (StringUtils.equalsIgnoreCase(argument, CD_UP)) {
            return goLower();
        } else {
            return goHigher(argument);
        }
    }

    private boolean goHigher(String directoryName) {
        if (StringUtils.isBlank(directoryName)) {
            logger.error("Please specify non empty directory name.");
            return true;
        }

        Path newPath = Path.of(currentPath.toAbsolutePath().toString(), directoryName);
        if (!Files.exists(newPath) || !Files.isDirectory(newPath)) {
            logger.error("Please specify existing directory.");
            return true;
        }

        currentPath = newPath;
        return true;
    }

    private boolean goLower() {
        if (currentPath.getParent() == null) {
            logger.error("Current directory doesn't have parent directory.");
            return true;
        }

        currentPath = currentPath.getParent();
        return true;
    }

    private void printCurrentPath() {
        logger.info("Current directory: {}", currentPath.toString());
    }

}
