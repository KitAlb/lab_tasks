package com.epam.lab.json.orangery;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Flower {

    private String Name;
    @JsonProperty("soil")
    private SoilType soil;
    private String origin;
    @JsonProperty("visual_parameters")
    private VisualParameters visualParameters;
    @JsonProperty("growing_tips")
    private GrowingTips growingTips;
    @JsonProperty("multiplying")
    private MultiplyingType multiplying;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public SoilType getSoil() {
        return soil;
    }

    public void setSoil(SoilType soil) {
        this.soil = soil;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public VisualParameters getVisualParameters() {
        return visualParameters;
    }

    public void setVisualParameters(VisualParameters visualParameters) {
        this.visualParameters = visualParameters;
    }

    public GrowingTips getGrowingTips() {
        return growingTips;
    }

    public void setGrowingTips(GrowingTips growingTips) {
        this.growingTips = growingTips;
    }

    public MultiplyingType getMultiplying() {
        return multiplying;
    }

    public void setMultiplying(MultiplyingType multiplying) {
        this.multiplying = multiplying;
    }

    @Override
    public String toString() {
        return "Flower{" +
                "Name='" + Name + '\'' +
                ", soil=" + soil +
                ", origin='" + origin + '\'' +
                ", visualParameters=" + visualParameters +
                ", growingTips=" + growingTips +
                ", multiplying=" + multiplying +
                '}';
    }
}
