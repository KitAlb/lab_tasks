package com.epam.lab.Ship_With_Droids;

import java.io.Serializable;

public abstract class Droid implements Comparable<Droid>, Serializable {

    transient String serialNumber;
    String name;
    int power;

    public Droid () {

    }

    public Droid (String name, int power) {
        this.name = name;
        this.power = power;
    }

    public static String getNameAndPower(Droid droid) {
        return(droid.name + " - " + droid.power);
    }

    @Override
    public String toString() {
        return this.name + " - " + this.power;
    }
}
