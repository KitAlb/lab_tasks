package com.epam.lab.pizza_store.store.dnipro;

import com.epam.lab.pizza_store.ingredients.PizzaIngredientsFactory;
import com.epam.lab.pizza_store.pizza.*;
import com.epam.lab.pizza_store.pizza.type.PizzaType;
import com.epam.lab.pizza_store.store.PizzaStore;
import org.apache.logging.log4j.Logger;

import java.util.Optional;

public class DniproPizzaStore extends PizzaStore {
    public DniproPizzaStore(Logger logger) {
        super(logger);
    }

    @Override
    protected Optional<Pizza> createPizza(PizzaType type) {
        PizzaIngredientsFactory ingredientsFactory = new DniproPizzaIngredientsFactory();

        switch (type) {
            case CHEESE:
                return Optional.of(new CheesePizza(
                        logger,
                        "Dnipro Style Cheese",
                        ingredientsFactory
                ));
            case CLAM:
                return Optional.of(new ClamPizza(
                        logger,
                        "Dnipro Style Clam",
                        ingredientsFactory
                ));
            case VEGGIE:
                return Optional.of(new VeggiesPizza(
                        logger,
                        "Dnipro Style Veggies",
                        ingredientsFactory
                ));
            case PEPPERONI:
                return Optional.of(new PepperoniPizza(
                        logger,
                        "Dnipro Style Pepperoni",
                        ingredientsFactory
                ));
            default:
                return Optional.empty();
        }
    }
}
