package com.epam.lab.Exceptions_Plants.enums;

import com.epam.lab.Exceptions_Plants.exceptions.TypeException;

import java.util.Arrays;

public enum Type {
    FLOWER("flower"),
    CACTUS("cactus"),
    TREE("tree"),
    MOSS("moss");

    private final String value;

    Type(final String value) {
        this.value = value;
    }

    public static Type of(String string) {
        return Arrays.stream(values())
                .filter(type -> type.toString().equalsIgnoreCase(string))
                .findFirst()
                .orElseThrow(() -> new TypeException(string));
    }

    @Override
    public String toString() {
        return value;
    }
}

