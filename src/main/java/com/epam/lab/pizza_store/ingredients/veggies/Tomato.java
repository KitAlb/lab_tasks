package com.epam.lab.pizza_store.ingredients.veggies;

public class Tomato implements Veggies {
    @Override
    public String toString() {
        return "Tomato";
    }
}
