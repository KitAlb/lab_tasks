package com.epam.lab.OOP_task_Necklace.jewels;

public class Ruby extends Jewel {

    private static final int PRICE_PER_KAR = 10;
    private static final int TRANSPARENCY = 50;

    public Ruby(int weight) {
        this(PRICE_PER_KAR, weight, TRANSPARENCY);
    }

    private Ruby(int pricePerKar, int weight, int transparency) {
        super(pricePerKar, weight, transparency);
    }

}
