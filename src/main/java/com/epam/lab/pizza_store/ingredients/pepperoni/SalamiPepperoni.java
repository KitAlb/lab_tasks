package com.epam.lab.pizza_store.ingredients.pepperoni;

public class SalamiPepperoni implements Pepperoni {
    @Override
    public String toString() {
        return "Salami";
    }
}
