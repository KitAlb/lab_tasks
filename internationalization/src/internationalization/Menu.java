package internationalization;

import java.util.HashMap;
import java.util.Map;

public class Menu {
    private Map<Choice, Runnable> choices = new HashMap<Choice, Runnable>();



    public Menu() {
        choices.put(Choice.CHOICE1, this::choiceMethod1);
        choices.put(Choice.CHOICE2, this::choiceMethod2);
    }

    public Runnable getChoice(Choice choice) {
        return choices.get(choice);
    }

    private void choiceMethod1() {
        System.out.println("Choice 1");
    }

    private void choiceMethod2() {
        System.out.println("Choice 2");
    }
}
