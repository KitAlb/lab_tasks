package com.epam.lab.jira.state;

import com.epam.lab.jira.task.Task;

public class Verified implements State {

    public void open(Task task) {
        task.setState(new Open());
        System.out.println("Reject verification");
    }

    public void close(Task task) {
        task.setState(new Closed());
        System.out.println("Close");
    }

}
