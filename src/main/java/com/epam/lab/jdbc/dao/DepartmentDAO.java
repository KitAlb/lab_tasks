package com.epam.lab.jdbc.dao;

import com.epam.lab.jdbc.model.DepartmentEntity;

public interface DepartmentDAO extends GeneralDAO<DepartmentEntity, String> {
}


