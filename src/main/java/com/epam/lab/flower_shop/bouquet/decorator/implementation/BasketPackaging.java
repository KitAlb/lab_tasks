package com.epam.lab.flower_shop.bouquet.decorator.implementation;

import com.epam.lab.flower_shop.bouquet.Bouquet;
import com.epam.lab.flower_shop.bouquet.decorator.BouquetDecorator;
import com.epam.lab.flower_shop.decorations.Basket;

public class BasketPackaging extends BouquetDecorator {
    public BasketPackaging(Bouquet bouquet) {
        super(bouquet);
        addDecoration(new Basket());
    }
}
