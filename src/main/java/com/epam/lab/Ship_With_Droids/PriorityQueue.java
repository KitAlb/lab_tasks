package com.epam.lab.Ship_With_Droids;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class PriorityQueue<T extends Droid> {

    private List<Droid> droids = new ArrayList<>();
    private Comparator<Droid> comparator;

    public PriorityQueue(Comparator<Droid> comparator) {
        this.comparator = comparator;
    }

    public void add(Droid droid) {
        droids.add(droid);
        droids.sort(comparator);
    }

    public Droid peek() {
        return this.droids.get(0);
    }

    public Droid poll() {
        return this.droids.remove(0);
    }

}
