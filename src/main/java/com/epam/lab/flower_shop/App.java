package com.epam.lab.flower_shop;

import com.epam.lab.flower_shop.bouquet.Bouquet;
import com.epam.lab.flower_shop.bouquet.decorator.implementation.BasketPackaging;
import com.epam.lab.flower_shop.bouquet.decorator.implementation.Delivery;
import com.epam.lab.flower_shop.bouquet.decorator.implementation.FuneralDecorator;
import com.epam.lab.flower_shop.bouquet.decorator.implementation.GoldCard;
import com.epam.lab.flower_shop.bouquet.implementation.RosesBouquet;
import com.epam.lab.flower_shop.decorations.Decoration;
import com.epam.lab.flower_shop.flowers.Flower;
import com.epam.lab.flower_shop.order.Order;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.stream.Collectors;

public class App {

    private static Logger logger = LogManager.getLogger("ConsoleLogger");

    public static void main(String[] args) {
        logger.info("Create order");
        Order newOrder = new Order(logger);
        newOrder.pay();
        logger.info("Prepare bouquet in basket for funeral having GoldMember card");
        Bouquet bouquet = new GoldCard(
                new Delivery(
                        new FuneralDecorator(
                                new BasketPackaging(
                                        new RosesBouquet(12)
                                )
                        )
                )
        );
        logger.info("Decorated bouquet price: {} tuhricks", bouquet.getPrice());
        logger.info("Flowers:");
        bouquet.getFlowers().stream()
                .collect(Collectors.groupingBy(Flower::getClass))
                .forEach((key, value) -> logger.info("    {}s - {}", key.getSimpleName(), value.size()));
        logger.info("Decorations: ");
        bouquet.getDecorations().stream()
                .collect(Collectors.groupingBy(Decoration::getClass))
                .forEach((key, value) -> logger.info("    {}s - {}", key.getSimpleName(), value.size()));
        newOrder.prepare(List.of(bouquet));
        newOrder.deliver();
        newOrder.close();
    }

}
