package com.epam.lab.pizza_store.ingredients.cheese;

public class Brynza implements Cheese {
    @Override
    public String toString() {
        return "Brynza";
    }
}
