package com.epam.lab.Ship_With_Droids;

import org.apache.commons.lang3.StringUtils;

public class C3PO extends Droid {
    public C3PO() {
        super("C-3P0", 40);
        this.serialNumber = "c3p0sn";
    }

    @Override
    public int compareTo(Droid o) {
        return StringUtils.compare(this.name, o.name);
    }
}
