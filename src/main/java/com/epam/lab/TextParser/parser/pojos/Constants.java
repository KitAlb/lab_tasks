package com.epam.lab.TextParser.parser.pojos;

import com.google.common.base.Strings;

public class Constants {

    public static String NON_ALPHABET_REGEX = "\\W+";
    public static String VOWELS = "aeiouy";
    public static String PLACEHOLDER = Strings.repeat("X", 7);
}
