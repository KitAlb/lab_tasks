package com.epam.lab.pizza_store.ingredients.sauce;

public class MarinaraSauce implements Sauce {
    @Override
    public String toString() {
        return "Marinara";
    }
}
