package com.epam.lab.flower_shop.bouquet.decorator.implementation;

import com.epam.lab.flower_shop.bouquet.Bouquet;

import static com.epam.lab.flower_shop.bouquet.Constants.DEFAULT_DELIVERY;

public class GoldCard extends Discount {
    public GoldCard(Bouquet bouquet) {
        super(bouquet, DEFAULT_DELIVERY);
    }
}
