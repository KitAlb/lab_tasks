package com.epam.lab.mobile_network.model;

import com.epam.lab.jdbc.model.annotation.PrimaryKey;
import com.epam.lab.mobile_network.model.annotation.Column;
import com.epam.lab.mobile_network.model.annotation.Table;

import java.time.LocalDateTime;
import java.util.UUID;

@Table(name = "notification")
public class Notification {
    @PrimaryKey
    @Column(name = "id")
    private UUID id;
    @Column(name = "user_id")
    private UUID userId;
    @Column(name = "trigger_date")
    private LocalDateTime triggerDate;
    @Column(name = "notification_text")
    private String notificationText;

    private Notification() {
    }

    Notification(UUID id, UUID userId, LocalDateTime triggerDate, String notificationText) {
        this.id = id;
        this.userId = userId;
        this.triggerDate = triggerDate;
        this.notificationText = notificationText;
    }

    public static NotificationBuilder builder() {
        return new NotificationBuilder();
    }

    public UUID getId() {
        return id;
    }

    public UUID getUserId() {
        return userId;
    }

    public LocalDateTime getTriggerDate() {
        return triggerDate;
    }

    public String getNotificationText() {
        return notificationText;
    }

    @Override
    public String toString() {
        return "Notification{" +
                "id=" + id +
                ", userId=" + userId +
                ", triggerDate=" + triggerDate +
                ", notificationText='" + notificationText + '\'' +
                '}';
    }

    public static class NotificationBuilder {
        private UUID id;
        private boolean isIdSet;
        private UUID userId;
        private LocalDateTime triggerDate;
        private String notificationText;

        public Notification.NotificationBuilder id(UUID id) {
            this.id = id;
            this.isIdSet = true;
            return this;
        }

        public Notification.NotificationBuilder userId(UUID userId) {
            this.userId = userId;
            return this;
        }

        public Notification.NotificationBuilder triggerDate(LocalDateTime triggerDate) {
            this.triggerDate = triggerDate;
            return this;
        }

        public Notification.NotificationBuilder notificationText(String text) {
            this.notificationText = text;
            return this;
        }

        public Notification build() {
            UUID id = this.id;
            if(!isIdSet) {
                id = UUID.randomUUID();
            }

            return new Notification(id, this.userId, this.triggerDate, this.notificationText);
        }
    }
}
