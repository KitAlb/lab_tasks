package com.epam.lab.json.orangery;

import com.fasterxml.jackson.annotation.JsonProperty;

public class VisualParameters {

    @JsonProperty("stem_color")
    private String stemColor;
    @JsonProperty("leaves_color")
    private String leavesColor;
    @JsonProperty("average_size")
    private int averageSize;

    public String getStemColor() {
        return stemColor;
    }

    public void setStemColor(String stemColor) {
        this.stemColor = stemColor;
    }

    public String getLeavesColor() {
        return leavesColor;
    }

    public void setLeavesColor(String leavesColor) {
        this.leavesColor = leavesColor;
    }

    public int getAverageSize() {
        return averageSize;
    }

    public void setAverageSize(int averageSize) {
        this.averageSize = averageSize;
    }

    @Override
    public String toString() {
        return "VisualParameters{" +
                "stemColor='" + stemColor + '\'' +
                ", leavesColor='" + leavesColor + '\'' +
                ", averageSize=" + averageSize +
                '}';
    }
}
