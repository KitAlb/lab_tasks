package com.epam.lab.flower_shop.bouquet.decorator.implementation;

import com.epam.lab.flower_shop.bouquet.Bouquet;
import com.epam.lab.flower_shop.bouquet.decorator.BouquetDecorator;

import static com.epam.lab.flower_shop.bouquet.Constants.DEFAULT_DELIVERY;

public class Delivery extends BouquetDecorator {
    private final double ADDITIONAL_PRICE = DEFAULT_DELIVERY;

    public Delivery(Bouquet bouquet) {
        super(bouquet);
        setAdditionalPrice(ADDITIONAL_PRICE);
    }
}
