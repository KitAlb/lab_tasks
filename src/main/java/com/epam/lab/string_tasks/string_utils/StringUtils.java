package com.epam.lab.string_tasks.string_utils;

public class StringUtils {
    private String string = "";

    public String concatenate(Object... args) {
        for (Object arg : args) {
            string = string + arg;
        }
        return string;
    }
}
