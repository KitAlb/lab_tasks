package com.epam.lab.Exceptions_Rectangle;

import com.epam.lab.Exceptions_Rectangle.rectangle.Rectangle;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.println("Please enter a and b:");
        Scanner sc = new Scanner(System.in);
        try {
            int a = sc.nextInt();
            int b = sc.nextInt();
            System.out.println("The result is " + Rectangle.squareRectangle(a, b));
        } catch (InputMismatchException e) {
            System.out.println("Non numerical value");
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }
    }

}



