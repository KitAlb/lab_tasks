package com.epam.lab.generic_tree;

import java.util.Comparator;

public class SimpleTreeMap<K,V> {

    private Entry<K,V> root;
    private final Comparator<K> comparator;

    SimpleTreeMap(Comparator<K> comparator) {
        this.comparator = comparator;
    }

    public void put(K key, V value) {
        if (root == null) {
            root = new Entry<K, V>(key, value, comparator);
        } else {
            root.put(key, value);
        }
    }

    public V get(K key ) {
        return root == null ? null : root.get(key);
    }
}
