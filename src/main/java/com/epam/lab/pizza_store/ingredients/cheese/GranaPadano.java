package com.epam.lab.pizza_store.ingredients.cheese;

public class GranaPadano implements Cheese {
    @Override
    public String toString() {
        return "Grana Padano";
    }
}
