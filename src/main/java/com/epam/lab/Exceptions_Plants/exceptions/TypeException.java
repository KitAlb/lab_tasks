package com.epam.lab.Exceptions_Plants.exceptions;

import com.epam.lab.Exceptions_Plants.enums.Type;

import java.util.Arrays;

public class TypeException extends RuntimeException {

    public TypeException(String typeString) {
        super("Unknown " + Type.class.getSimpleName() +
                " - " + typeString + ", allowed values are " + Arrays.toString(Type.values()));
    }
}
