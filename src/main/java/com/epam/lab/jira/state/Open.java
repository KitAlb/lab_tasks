package com.epam.lab.jira.state;

import com.epam.lab.jira.task.Task;

public class Open implements State {

    public void progress(Task task) {
        task.setState(new InProgress());
        System.out.println("Start work");
    }

    public void block(Task task) {
        task.setState(new Blocked());
        System.out.println("Request info");
    }

    public void resolve(Task task) {
        task.setState(new Resolved());
        System.out.println("Mark as duplicate");
    }

}
