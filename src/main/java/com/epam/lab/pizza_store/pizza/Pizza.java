package com.epam.lab.pizza_store.pizza;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Stream;

import com.epam.lab.pizza_store.ingredients.cheese.Cheese;
import com.epam.lab.pizza_store.ingredients.clams.Clams;
import com.epam.lab.pizza_store.ingredients.dough.Dough;
import com.epam.lab.pizza_store.ingredients.pepperoni.Pepperoni;
import com.epam.lab.pizza_store.ingredients.sauce.Sauce;
import com.epam.lab.pizza_store.ingredients.veggies.Veggies;
import org.apache.logging.log4j.Logger;

public abstract class Pizza {
    protected Logger logger;

    protected String name;
    protected Dough dough;
    protected Sauce sauce;
    protected List<Cheese> cheeses = new ArrayList<>();
    protected List<Veggies> veggies = new ArrayList<>();
    protected List<Clams> clams = new ArrayList<>();
    protected List<Pepperoni> pepperonis = new ArrayList<>();

    public Pizza(Logger logger, String name) {
        this.name = name;
        this.logger = logger;
    }

    public String getName() {
        return name;
    }

    public abstract void prepare();

    public void bake() {
        logger.info("Bake for 5 minutes at 400 C");
    }

    public void cut() {
        logger.info("Cutting into diagonal slices");
    }

    public void box() {
        logger.info("Place in official PizzaStore box");
    }

    public String toString() {
        StringJoiner joiner = new StringJoiner("\n");
        joiner.add("Dough: " + dough);
        joiner.add("Sauce: " + sauce);
        joiner.add("Toppings:");

        Stream.of(
                cheeses.stream(),
                veggies.stream(),
                clams.stream(),
                pepperonis.stream()
        ).flatMap(i -> i)
                .forEach(item -> joiner.add("    " + item));

        return joiner.toString();
    }

}
