package com.epam.lab.flower_shop.order.state;

import com.epam.lab.flower_shop.order.Order;

public class DeliveredState implements State {
    @Override
    public String close(Order order) {
        order.setState(new ClosedState());
        return "Order was completed.";
    }
}
