package com.epam.lab.jdbc.dao;

import com.epam.lab.jdbc.model.EmployeeEntity;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.List;

public interface EmployeeDAO extends GeneralDAO<EmployeeEntity, Integer> {
    List<EmployeeEntity> findByName(String name) throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException;
    List<EmployeeEntity> findByDeptNo(String deptNo) throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException;
}
