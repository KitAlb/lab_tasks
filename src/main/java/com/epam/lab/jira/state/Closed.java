package com.epam.lab.jira.state;

import com.epam.lab.jira.task.Task;

public class Closed implements State {

    public void open(Task task) {
        task.setState(new Open());
        System.out.println("Reopen issue");
    }

}
