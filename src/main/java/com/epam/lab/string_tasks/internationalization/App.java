package com.epam.lab.string_tasks.internationalization;

import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Scanner;

public class App {
    private static Menu menu = new Menu();

    public static void main(String[] args) {

        String language;
        String country;

        if (args.length != 2) {
            language = new String("en");
            country = new String("US");
        } else {
            language = new String(args[0]);
            country = new String(args[1]);
        }

        Locale currentLocale;
        ResourceBundle messages;

        currentLocale = new Locale(language, country);

        messages = ResourceBundle.getBundle(".MessagesBundle", currentLocale);

        Scanner scanner = new Scanner(System.in);

        for (Choice value : Choice.values()) {
            System.out.println(value);
        }

        System.out.print(messages.getString("greeting"));
        int choiceIndex = scanner.nextInt();

        if (choiceIndex < 0 || choiceIndex > Choice.values().length) {

            System.out.println("Please choose right value");
        }

        menu.getChoice(Choice.values()[choiceIndex - 1]).run();

    }
}
