package com.epam.lab.annotation;

import java.util.Arrays;

public class ClassInfo {

    private Object clazz;

    public ClassInfo(Object clazz) {
        this.clazz = clazz;
    }

    public void printInfo() {
        Class<?> c = clazz.getClass();

        System.out.println("Simple name: " + c.getSimpleName());
        System.out.println("Canonical name: " + c.getCanonicalName());
        System.out.println("Constructors: " + Arrays.toString(c.getDeclaredConstructors()));
        System.out.println("Methods: " + Arrays.toString(c.getDeclaredMethods()));
        System.out.println("Fields: " + Arrays.toString(c.getDeclaredFields()));
    }

}
