package com.epam.lab.folders_content;

import java.util.Arrays;

public enum Command {

    DIR("dir"),
    CD("cd"),
    EXIT("exit");

    private String value;

    Command(String value) {
        this.value = value;
    }

    public static Command of(String string) {
        return Arrays.stream(Command.values())
                .filter(item -> item.value.equalsIgnoreCase(string))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Invalid command. Allowed values are: " +
                        Arrays.toString(Command.values())));
    }

    @Override
    public String toString() {
        return value;
    }

}
