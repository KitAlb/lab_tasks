package com.epam.lab.OOP_task_Necklace.generator;

import com.epam.lab.OOP_task_Necklace.jewels.*;
import com.epam.lab.OOP_task_Necklace.jewels.type.JewelType;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

public class JewelsGenerator {

    private static final int MAX_WEIGHT = 100;
    private static final int MAX_COUNT = 10;

    public static List<Jewel> getRandomJewels() {
        List<Jewel> jewels = new ArrayList<>();
        int count = new Random().nextInt(MAX_COUNT);

        for (int i = 0; i < count; i++) {
            getRandomJewel().ifPresent(jewels::add);
        }

        return jewels;
    }

    public static Optional<Jewel> getRandomJewel() {
        JewelType jewelType = JewelType.values()[new Random().nextInt(JewelType.values().length)];
        int weight = new Random().nextInt(MAX_WEIGHT);

        switch (jewelType) {
            case Opal:
                return Optional.of(new Opal(weight));
            case Ruby:
                return Optional.of(new Ruby(weight));
            case Sapphire:
                return Optional.of(new Sapphire(weight));
        }

        return Optional.empty();
    }

}