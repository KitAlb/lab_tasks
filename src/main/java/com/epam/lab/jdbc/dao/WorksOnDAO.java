package com.epam.lab.jdbc.dao;

import com.epam.lab.jdbc.model.PK_WorksOn;
import com.epam.lab.jdbc.model.WorksOnEntity;

public interface WorksOnDAO extends GeneralDAO<WorksOnEntity, PK_WorksOn> {
}
