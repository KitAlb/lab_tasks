package com.epam.lab.Comparable.generator;

import com.epam.lab.Comparable.boardgame.Boardgame;
import org.apache.commons.lang3.RandomStringUtils;

public class BoardgameGenerator {

    public static Boardgame getRandomBoardgame() {
        return new Boardgame(
                RandomStringUtils.random(10, true, false),
                RandomStringUtils.random(10, true, false)
        );
    }

}
