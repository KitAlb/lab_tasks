package com.epam.lab.jira.state;

import com.epam.lab.jira.task.Task;

public class InProgress implements State {

    public void open(Task task) {
        task.setState(new Open());
        System.out.println("Stop work");
    }

    public void block(Task task) {
        task.setState(new Blocked());
        System.out.println("Request info");
    }

    public void resolve(Task task) {
        task.setState(new Resolved());
        System.out.println("Mark as duplicate");
    }

    public void review(Task task) {
        task.setState(new InReview());
        System.out.println("Submit for review");
    }

}
